# Initial Setup

1. Use node version 18: 
   1. Install nvm for node version control: [https://github.com/nvm-sh/nvm](https://github.com/nvm-sh/nvm)
   2. Install node version 18.13.0 (for example): `nvm install 18.13.0` 
   3. Use node version 18.13.0: `nvm use 18.13.0`
2. Install firebase command line tools: `npm i -g firebase-tools` 
3. Log into your firebae account: `firebase login`
4. Select the project: `firebase init`

# Testing the project locally

1. Build JS bundle: `npm run-script build`
2. Serve the functions locally: `firebase serve --only functions`
/* tslint:disable */
import * as eccrypto from "@toruslabs/eccrypto";
import * as CryptoJS from 'crypto-js';
const crypto = require("crypto");
import atob = require("atob");
import btoa = require("btoa");

const uInt8ArrayToBase64 = (bytes: Uint8Array): string => {
  let binary = "";
  const len = bytes.length;
  for (let i = 0; i < len; i++) {
    binary += String.fromCharCode(bytes[i]);
  }
  return btoa(binary);
};

const base64ToUint8Array = (base64: string): Uint8Array => {
  const binaryString = atob(base64);
  const len = binaryString.length;
  const bytes = new Uint8Array(len);
  for (let i = 0; i < len; i++) {
    bytes[i] = binaryString.charCodeAt(i);
  }
  return bytes;
};

const base64ToHex = (str: string): string => {
  const raw = atob(str);
  let result = "";
  for (let i = 0; i < raw.length; i++) {
    const hex = raw.charCodeAt(i).toString(16);
    result += hex.length === 2 ? hex : "0" + hex;
  }
  return result.toUpperCase();
};

const uIntToBuffer = (u: Uint8Array): Buffer => {
  return Buffer.from(
    base64ToHex(uInt8ArrayToBase64(u)),
    "hex"
  );
};

const base64Tobase64url = (input) => {
  if(input){
      input = input
      .replace(/\+/g, '-')
      .replace(/\//g, '_');
  }
  return input;
}

const hex2i = (hexString) => {
  return new Uint8Array(hexString.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));
}

const base64urlTobase64 = (input) => {
  if (input) {
    input = input.replace(/-/g, '+').replace(/_/g, '/');
    const pad = input.length % 4;
    if (pad) {
      if (pad === 1) {
        throw new Error('InvalidLengthError: Input base64url string is the wrong length to determine padding');
      }
      input += new Array(5 - pad).join('=');
    }
  }
  return input;
}

const privateKey = process.env.PRIVATE_KEY;

export const generateBlueTokens = (): Promise<{
  key: string;
  encrypted: string;
}> => {
  return new Promise((resolve) => {
      var r = CryptoJS.lib.WordArray.random(32).toString();
      var rm = new Uint8Array(r.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));
      var m = process.env.BLUE_TOKEN;
      var mm = Buffer.from(m, 'hex');
      eccrypto.encrypt(mm, Buffer.from(rm)).then(encrypted => {
          var encryptedUnion = Buffer.concat([ encrypted.iv, encrypted.ephemPublicKey, encrypted.ciphertext, encrypted.mac ]);
          var header = base64Tobase64url(uInt8ArrayToBase64(encryptedUnion));
          resolve({
              key: r,
              encrypted: header
          })
      });
  })
}

export const decryptBody = async (encrypted: string): Promise<string> => {
  const decryptedUnion = base64ToUint8Array(encrypted);

  const arrayByteObject = {
    iv: decryptedUnion.slice(0, 16),
    v: decryptedUnion.slice(16, 16 + 65),
    cipherText: decryptedUnion.slice(16 + 65, decryptedUnion.length - 32),
    t: decryptedUnion.slice(decryptedUnion.length - 32),
  };

  const hexByteObjectEncrypted = {
    iv: uIntToBuffer(arrayByteObject.iv),
    ephemPublicKey: uIntToBuffer(arrayByteObject.v),
    ciphertext: uIntToBuffer(arrayByteObject.cipherText),
    mac: uIntToBuffer(arrayByteObject.t),
  };

  const decryptedData = await eccrypto.decrypt(
    Buffer.from(privateKey, "hex"),
    hexByteObjectEncrypted
  );

  return new TextDecoder().decode(decryptedData);
};

export const symmetricDecrypt = (text: string, symmetricKey: string) => {
  var hexwith0C = base64ToHex(base64urlTobase64(text));
  var byteArrayResult = hex2i(hexwith0C.slice(2));

  var byteArrayObject = {
      iv: byteArrayResult.slice(0, 12),
      ciphertext: byteArrayResult.slice(12, byteArrayResult.length - 16),
      authTag: byteArrayResult.slice(byteArrayResult.length - 16)
  };
  var byteArrayObjectHex = {
      iv: Buffer.from(base64ToHex(uInt8ArrayToBase64(byteArrayObject.iv)), 'hex'),
      ciphertext: Buffer.from(base64ToHex(uInt8ArrayToBase64(byteArrayObject.ciphertext)), 'hex'),
      authTag: Buffer.from(base64ToHex(uInt8ArrayToBase64(byteArrayObject.authTag)), 'hex')
  };
  var key = Buffer.from(symmetricKey, 'hex');
  var decipher = crypto.createDecipheriv('aes-256-gcm', key, byteArrayObjectHex.iv);
  decipher.setAuthTag(byteArrayObjectHex.authTag);
  let decrypted = decipher.update(byteArrayObjectHex.ciphertext, 'hex', 'utf8');
  decrypted += decipher.final();
  return decrypted;
}

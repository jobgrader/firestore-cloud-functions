/* tslint:disable */
import * as eccrypto from "@toruslabs/eccrypto";;

import btoa = require("btoa");

const publicKey = process.env.PUBLIC_KEY;

export const encryptBody = async (body: any): Promise<string> => {
  const stringified = JSON.stringify(body);

  const encryptedBuffer = await eccrypto.encrypt(
    Buffer.from(publicKey, "hex"),
    Buffer.from(stringified)
  );

  const uInt8ArrayToBase64 = (bytes) => {
    let binary = "";
    const len = bytes.length;
    for (let i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return btoa(binary);
  };

  const encryptedUnion = Buffer.concat([
    encryptedBuffer.iv,
    encryptedBuffer.ephemPublicKey,
    encryptedBuffer.ciphertext,
    encryptedBuffer.mac,
  ]);

  return uInt8ArrayToBase64(encryptedUnion);
};

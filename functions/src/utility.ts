/* tslint:disable */

import { Timestamp } from "firebase-admin/firestore";
const { createHash, createHmac } = require('crypto');

export interface TelegramUser {
  id: number;
  first_name: string;
  last_name: string;
  username: string;
  photo_url: string;
  auth_date: number;
  hash: string;
}

interface Trust {
  gender?: any;
  title?: any;
  firstname?: any;
  middlename?: any;
  lastname?: any;
  maritalstatus?: any;
  emailtype?: any;
  email?: any;
  phonetype?: any;
  phone?: any;
  dateofbirth?: any;
  cityofbirth?: any;
  countryofbirth?: any;
  citizenship?: any;
  street?: any;
  city?: any;
  zip?: any;
  state?: any;
  country?: any;
  identificationdocumenttype?: any;
  identificationdocumentnumber?: any;
  identificationissuecountry?: any;
  identificationissuedate?: any;
  identificationexpirydate?: any;
  driverlicencedocumentnumber?: any;
  driverlicencecountry?: any;
  driverlicenceissuedate?: any;
  driverlicenceexpirydate?: any;
  passportnumber?: any;
  passportissuecountry?: any;
  passportissuedate?: any;
  passportexpirydate?: any;
  residencepermitnumber?: any;
  residencepermitissuecountry?: any;
  residencepermitissuedate?: any;
  residencepermitexpirydate?: any;
  taxresidency?: any;
  taxnumber?: any;
  termsofuse?: any;
  termsofusethirdparties?: any;
  maidenname?: any;
}

export const mapTrustData = (trust) => {
  const setTrustData = ((trustData: Trust) => (key: keyof Trust) => {
    return !!trustData[key] ? (trustData[key].trustActive ? trustData[key].trustValue : 0) : 0;
  })(trust);
  return {
    emailStatus: 1,
    genderStatus: setTrustData('gender'),
    maritalstatusStatus: setTrustData('maritalstatus'),
    firstnameStatus: setTrustData('firstname'),
    lastnameStatus: setTrustData('lastname'),
    dateofbirthStatus: setTrustData('dateofbirth'),
    countryofbirthStatus: setTrustData('countryofbirth'),
    citizenshipStatus: setTrustData('citizenship'),
    streetStatus: setTrustData('street'),
    cityStatus: setTrustData('city'),
    zipStatus: setTrustData('zip'),
    stateStatus: setTrustData('state'),
    cityofbirthStatus: setTrustData('cityofbirth'),
    countryStatus: setTrustData('country'),
    maidennameStatus: setTrustData('maidenname'),
    middlenameStatus: setTrustData('middlename'),
    identificationdocumentnumberStatus: setTrustData('identificationdocumentnumber'),
    identificationissuecountryStatus: setTrustData('identificationissuecountry'),
    identificationexpirydateStatus: setTrustData('identificationexpirydate'),
    identificationissuedateStatus: setTrustData('identificationissuedate'),
    driverlicencedocumentnumberStatus: setTrustData('driverlicencedocumentnumber'),
    driverlicencecountryStatus: setTrustData('driverlicencecountry'),
    driverlicenceexpirydateStatus: setTrustData('driverlicenceexpirydate'),
    driverlicenceissuedateStatus: setTrustData('driverlicenceissuedate'),
    passportnumberStatus: setTrustData('passportnumber'),
    passportissuecountryStatus: setTrustData('passportissuecountry'),
    passportexpirydateStatus: setTrustData('passportexpirydate'),
    passportissuedateStatus: setTrustData('passportissuedate'),
    residencepermitnumberStatus: setTrustData('residencepermitnumber'),
    residencepermitissuecountryStatus: setTrustData('residencepermitissuecountry'),
    residencepermitexpirydateStatus: setTrustData('residencepermitexpirydate'),
    residencepermitissuedateStatus: setTrustData('residencepermitissuedate'),
    phoneStatus: setTrustData('phone'),
  };
}

export const hash = (s: string) => {
  return createHash('sha256').update(s).digest('hex');
}

export const randomString = (length: number) => {
  const pow1 = Math.pow(36, length + 1);
  const pow = Math.pow(36, length);
  const ran = Math.random();
  return Math.round((pow1 - ran * pow)).toString(36).slice(1);
};

export const processedTimestamp = (firestoreTimestamp: any) => {
  const seconds = firestoreTimestamp._seconds;
  const nanoseconds = firestoreTimestamp._nanoseconds;
  return (seconds * 1000) + (nanoseconds / 1000000);
}

export const convertTimestamp = (unixTimestamp: any) => {
  let r;
  try {
    r = new Timestamp(unixTimestamp, 0);
  } catch (error) {
    r = unixTimestamp;
  }
  return r;
}

export const validateTelegramHash = (user: TelegramUser, botToken: string) => {
  const secretKey = createHash('sha256').update(botToken).digest();
  const dataCheckString = Object.keys(user)
    .filter(key => key !== 'hash')
    .sort()
    .map(key => `${key}=${user[key as keyof TelegramUser]}`)
    .join('\n');

  const hmac = createHmac('sha256', secretKey)
    .update(dataCheckString)
    .digest('hex');

  return hmac === user.hash;
}

export const checkSolutionEquality = (storedSolution: string, providedSolution: string, minimumCorrectAnswersRequired: number): boolean => {
  let numberOfCorrectAnswers = 0;

  const aa = JSON.parse(storedSolution);
  const bb = JSON.parse(providedSolution);
  const l = Math.min(aa.length, bb.length);

  for (let i = 0; i < l; i++) {
    if (aa[i] == bb[i]) {
      numberOfCorrectAnswers++;
    }
  }

  return (numberOfCorrectAnswers >= minimumCorrectAnswersRequired);

}

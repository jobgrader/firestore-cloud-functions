/* tslint:disable */
import * as admin from "firebase-admin";
import { DecodedIdToken } from "firebase-admin/auth";
import { ethers } from "ethers";
import Jimp = require("jimp");
import { hash } from "./utility";
import { format } from "date-fns";
import { toZonedTime } from "date-fns-tz";
import { coreTemplate } from "./coreTemplate";
import { nftAbi } from "./nftAbi";

export const updateMintingLogs = async (userId, walletAddress, contractAddress, nftId, status) => {
	const doc = await admin.firestore().doc(`minting_logs/${userId}`).get();
	if (!doc.exists) {
		try {
			await admin.firestore().doc(`minting_logs/${userId}`).create({
				mints: [
					{
						walletAddress,
						contractAddress,
						status,
						nftId,
						timestamp: new Date().toISOString()
					}
				]
			})
		} catch (error) {
			console.log(error);
		}
	} else {
		const mints = doc.data().mints;
		const relevant = mints.find(m => m.nftId == nftId);
		if (relevant) {
			mints.find(m => m.nftId == nftId).status = status;
			mints.find(m => m.nftId == nftId).timestamp = new Date().toISOString();
		} else {
			mints.push({
				walletAddress,
				contractAddress,
				status,
				nftId,
				timestamp: new Date().toISOString()
			})
		}
		try {
			await admin.firestore().doc(`minting_logs/${userId}`).update({
				mints
			})
		} catch (error) {
			console.log(error);
		}
	}

}

const capitalizeName = (name) => {
	let lowerCaseName = name.toLowerCase();
	let nameParts = lowerCaseName.split(' ');
	for (let i = 0; i < nameParts.length; i++) {
		nameParts[i] = nameParts[i].charAt(0).toUpperCase() + nameParts[i].slice(1);
	}
	let capitalizedName = nameParts.join(' ');
	return capitalizedName;
}

export const addTextToImage = async ({ imagePath, name, title, nameFontPath, titleFontPath }, to, callname) => {

	return new Promise(async (resolve, reject) => {
		try {
			const image = await Jimp.read(imagePath);
			const nameFont = await Jimp.loadFont(nameFontPath);
			const titleFont = await Jimp.loadFont(titleFontPath);

			const capitalizedName = capitalizeName(name);

			const nameTextWidth = Jimp.measureText(nameFont, capitalizedName);
			const nameTextHeight = Jimp.measureTextHeight(nameFont, capitalizedName, 100);

			const titleTextWidth = Jimp.measureText(titleFont, title);
			const titleTextHeight = Jimp.measureTextHeight(titleFont, title, 100);

			const now = new Date();
			const utcTime = toZonedTime(now, "UTC");
			const date = format(utcTime, "HH:mm dd-MM-yyyy") + " UTC";
			// const date = `${now.getHours() < 10 ? `0${now.getHours()}` : now.getHours()}:${now.getMinutes() < 10 ? `0${now.getMinutes()}` : now.getMinutes()} ${now.getDate() < 10 ? `0${now.getDate()}` : now.getDate()}-${now.getMonth() < 9 ? `0${now.getMonth()+1}` : now.getMonth()+1}-${now.getFullYear()}`;

			const dateTextWidth = Jimp.measureText(titleFont, date);
			const dateTextHeight = Jimp.measureTextHeight(titleFont, date, 100);

			image.print(nameFont, Math.round((image.bitmap.width - nameTextWidth) / 2), Math.round((image.bitmap.height - nameTextHeight) / 2), capitalizedName);
			image.print(titleFont, Math.round((image.bitmap.width - titleTextWidth) / 2), Math.round((image.bitmap.height - titleTextHeight) / 2) + 160, title);
			image.print(titleFont, Math.round((image.bitmap.width - dateTextWidth) / 2), Math.round((image.bitmap.height - dateTextHeight) / 2) + 375, date);

			const buffer = await image.getBufferAsync(Jimp.MIME_PNG);

			// const base64 =  await image.getBase64Async(Jimp.MIME_PNG);
			// const innerHtml =  `<img src="${base64}" />`

			const content = `<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="bff8ffa1-41a9-4aab-a2ea-52ac3767c6f4" data-mc-module-version="2019-10-22">
    <tbody>
        <tr>
            <td style="padding:18px 30px 18px 30px; line-height:40px; text-align:inherit;" height="100%" valign="top" role="module-content">
            <div>
                <div style="font-family: inherit; text-align: center"><span style="font-size: 40px; font-family: inherit; color: #000000 !important;">
                    Hi ${callname}!
                    </span>
                </div>
                <div></div>
            </div>
            </td>
        </tr>
    </tbody>
</table>
<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="2f94ef24-a0d9-4e6f-be94-d2d1257946b0" data-mc-module-version="2019-10-22">
    <tbody>
        <tr>
            <td style="padding:18px 50px 18px 50px; line-height:40px; text-align:inherit;" height="100%" valign="top" role="module-content">
            <div>
                <div style="font-family: inherit; font-size: 40px; text-align: center; color: #000000 !important;">Congratulations! <br><br>Your soulbound NFT has been minted, and you receive the ${title} certificate!</div>
                <div></div>
            </div>
            </td>
        </tr>
    </tbody>
</table>`;

			const sha = hash(content + now);
			const html = coreTemplate(content, sha);

			admin.firestore().collection('mail').add({
				to,
				message: {
					subject: 'Your Soulbound NFT!',
					html,
					attachments: [{
						filename: 'certificate.png',
						content: buffer
					}]
				},
			}).then(() => {
				console.log("Queued email for delivery!");
				resolve(true);
			}).catch(e => {
				resolve(false);
			})

			console.log('Image with text saved successfully!');
		} catch (error) {
			console.error('Error adding text to image:', error);
		}
	})
}

export const fetchEpnNfts = async (ownerAddress) => {

	// const environment = (await admin.firestore().doc(`environment/assessment`).get()).data();

	// Set up the RPC URL and create a provider
	const rpcUrl = "https://core.evan.network";
	const provider = new ethers.JsonRpcProvider(rpcUrl);

	// Address of the NFT contract (Replace with the actual contract address)
	const nftContractAddress = "0x35003C3827cB7ac45ca7d61584b153b6C8ee0DfA";

	// Create a contract instance
	const nftContract = new ethers.Contract(nftContractAddress, nftAbi, provider);

	// Get the latest block number
	const latestBlock = await provider.getBlockNumber();

	// Filter Transfer events to get all minted NFTs
	const filter = nftContract.filters.Transfer(ethers.ZeroAddress, null);
	const events = await nftContract.queryFilter(filter, 0, latestBlock);

	// Extract token IDs and block numbers from the events
	const tokenData = events.map((event: any) => ({
		tokenId: event.args.tokenId,
		blockNumber: event.blockNumber
	}));

	const nfts = [];

	for (let { tokenId, blockNumber } of tokenData) {
		const tokenURI = await nftContract.tokenURI(tokenId);
		const owner = await nftContract.ownerOf(tokenId);

		if (owner.toLowerCase() == ownerAddress.toLowerCase()) {
			const metadata = await (await fetch(tokenURI)).json();

			const name = await nftContract.name();
			const symbol = await nftContract.symbol();

			// Get block details to fetch the timestamp
			const blockDetails = await provider.getBlock(blockNumber);
			const creationTimestamp = new Date(blockDetails.timestamp * 1000);

			nfts.push({
				token_address: nftContractAddress,
				token_id: Number(tokenId).toString(),
				contract_type: "ERC721",
				owner_of: owner,
				block_number: blockNumber,
				block_number_minted: blockNumber,
				token_uri: tokenURI,
				name,
				symbol,
				metadata,
				last_token_uri_sync: creationTimestamp,
				last_metadata_sync: creationTimestamp,
				possible_spam: false
			});
		}
	}

	// {
	//     "token_address": "0xb47e3cd837dDF8e4c57F05d70Ab865de6e193BBB",
	//     "token_id": "15",
	//     "contract_type": "ERC721",
	//     "owner_of": "0x057Ec652A4F150f7FF94f089A38008f49a0DF88e",
	//     "block_number": "88256",
	//     "block_number_minted": "88256",
	//     "token_uri": "",
	//     "metadata": "",
	//     "normalized_metadata": "",
	//     "media": "",
	//     "amount": "1",
	//     "name": "CryptoKitties",
	//     "symbol": "RARI",
	//     "token_hash": "502cee781b0fb40ea02508b21d319ced",
	//     "last_token_uri_sync": "2021-02-24T00:47:26.647Z",
	//     "last_metadata_sync": "2021-02-24T00:47:26.647Z",
	//     "possible_spam": "false",
	//     "verified_collection": "false"
	// }

	console.log(nfts);

	return nfts;

}

export const mintAssessmentNft = async (recipient, userId, email, epn, nftId): Promise<boolean> => {

	return new Promise(async (resolve) => {
		const environment = (await admin.firestore().doc(`environment/assessment`).get()).data();
		const solutions = (await admin.firestore().doc(`job_solutions/quiz`).get()).data().solutions;

		const privateKey = !epn ? process.env.WALLET_PRIVATE_KEY : process.env.HELIX_PRIVATE_KEY;
		const gnosisRpcUrl = !epn ? environment.rpc_url : "https://core.evan.network";
		const contractAddress = !epn ? environment.contract : "0x35003C3827cB7ac45ca7d61584b153b6C8ee0DfA";
		const tokenUri = solutions.find(s => s.nftId == nftId).token_uri;
		const gasLimit = !epn ? 17049572 : 210000;

		console.log({ epn, contractAddress, tokenUri, gnosisRpcUrl });

		updateMintingLogs(userId, recipient, contractAddress, environment.nftId, "Minting Started");

		const provider = new ethers.JsonRpcProvider(gnosisRpcUrl);
		const wallet = new ethers.Wallet(privateKey, provider);

		const contract = new ethers.Contract(contractAddress, nftAbi, wallet);

		if (ethers.isAddress(recipient)) {
			try {
				const gasPrice = (await provider.getFeeData()).gasPrice;
				const tx = await contract.safeMint(recipient,
					tokenUri,
					{
						gasPrice,
						gasLimit
					});
				console.log("Minting NFT...");
				await tx.wait();
				console.log({ tx });
				console.log(`NFT minted to ${recipient}`);
				updateMintingLogs(userId, recipient, contractAddress, environment.nftId, "Minting Finished");
				resolve(true);
			} catch (error) {
				if (!epn) {
					try {
						updateMintingLogs(userId, recipient, contractAddress, environment.nftId, "Retrying...");

						const inputString = error.error.message; // e.g. "GasLimitExceeded, Gas limit: 17016568, gas limit of rejected tx: 17049572";
						const regex = /\b\d+\b/g;
						const numbers = inputString.match(regex).map(Number);

						console.log(`Retrying with gasLimit ${numbers[0]} instead of ${numbers[1]}`);
						const gasPrice = (await provider.getFeeData()).gasPrice;

						const tx = await contract.safeMint(recipient,
							tokenUri,
							{
								gasPrice,
								gasLimit: numbers[0]
							});
						console.log("Minting NFT...");
						await tx.wait();
						console.log({ tx });
						console.log(`NFT minted to ${recipient}`);
						updateMintingLogs(userId, recipient, contractAddress, environment.nftId, "Minting Finished");
						resolve(true);

					} catch (error2) {

						try {

							updateMintingLogs(userId, recipient, contractAddress, environment.nftId, "Retrying...");

							const inputString = error2.error.message; // e.g. "GasLimitExceeded, Gas limit: 17016568, gas limit of rejected tx: 17049572";
							const regex = /\b\d+\b/g;
							const numbers = inputString.match(regex).map(Number);

							console.log(`Retrying with gasLimit ${numbers[0]} instead of ${numbers[1]}`);
							const gasPrice = (await provider.getFeeData()).gasPrice;

							const tx = await contract.safeMint(recipient,
								tokenUri,
								{
									gasPrice,
									gasLimit: (numbers[0] * 1.2)
								});
							console.log("Minting NFT...");
							await tx.wait();
							console.log({ tx });
							console.log(`NFT minted to ${recipient}`);
							updateMintingLogs(userId, recipient, contractAddress, environment.nftId, "Minting Finished");
							resolve(true);

						} catch (error3) {

							updateMintingLogs(userId, recipient, contractAddress, environment.nftId, "Error in Minting");
							mintingErrorNotifier(recipient, email, contractAddress, environment.nftId, JSON.stringify(error3));
							resolve(false);

						}


					}
				} else {
					try {

						updateMintingLogs(userId, recipient, contractAddress, environment.nftId, "Retrying...");

						const gasPrice = (await provider.getFeeData()).gasPrice;
						const nonce = await provider.getTransactionCount(recipient) + 1;

						const tx = await contract.safeMint(recipient,
							tokenUri,
							{
								gasPrice: gasPrice * BigInt(12) / BigInt(10),
								gasLimit: (gasLimit * 1.2),
								nonce
							});
						console.log("Minting NFT...");
						await tx.wait();
						console.log({ tx });
						console.log(`NFT minted to ${recipient}`);
						updateMintingLogs(userId, recipient, contractAddress, environment.nftId, "Minting Finished");
						resolve(true);

					} catch (error4) {

						updateMintingLogs(userId, recipient, contractAddress, environment.nftId, "Error in Minting");
						mintingErrorNotifier(recipient, email, contractAddress, environment.nftId, JSON.stringify(error4));
						resolve(false);

					}
				}

			}

		} else {
			console.log(`Invalid address entered: ${recipient}`);
			updateMintingLogs(userId, recipient, contractAddress, environment.nftId, "Invalid Address Provided");
			mintingErrorNotifier(recipient, email, contractAddress, environment.nftId, "Invalid Address Provided");
			resolve(false);
		}
	})

}

const mintingErrorNotifier = (address, email, contract, nftId, reason) => {
	const now = new Date();
	const content = `<content>
	<div>
		<div>
			<div>
				<img style="max-width: 100%; padding: 10px; height: 40px;" src="https://jobgrader.app/wp-content/uploads/2023/05/Jobgrader_green.svg" alt="">
				<p style="font-family: 'Montserrat', sans-serif; font-weight: 350; font-size: 30px;">Minting Error!</p>
				<p style="font-family: 'Montserrat', sans-serif; font-weight: 300; font-size: 20px;">
					User's Contact Email: ${email}<br>
					Wallet Address: ${address}<br>
					Contract Address: ${contract}<br>
					NFT ID: ${nftId}<br>
					Reason: ${reason}<br>
					Timestamp: ${now.toISOString()}
				</p>
			</div>
		</div>
	</div>
</content>`;

	const sha = hash(content + now);
	const mailContent = `
	<!DOCTYPE html>
	<html lang="en">
	<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Minting Error!</title>
	</head>
	<body>
	<div style="
		display: contents;
		text-align: center;
		height: 100vh;
		max-width: 600em;
		background: white;
		padding: 5%;
		">
	${content}
	<footer style="background-color: #ececec !important; width: 100%; padding-top: 0.5%; padding-bottom: 0.5%;">		
		<p style="color: #a0a0a0 !important; font-family: 'Montserrat', sans-serif; font-size: 0.7rem;">
			<span ><a style="color: #a0a0a0 !important; font-family: 'Montserrat', sans-serif;" href="https://jobgrader.app/privacy-policy">Privacy Policy</a></span>
			<span><a style="color: #a0a0a0 !important; font-family: 'Montserrat', sans-serif;" href="https://jobgrader.app/imprint">Imprint</a></span>
			<span><a style="color: #a0a0a0 !important; font-family: 'Montserrat', sans-serif;" href="https://jobgrader.app/">Contact</a></span>
		</p>
		<p style="color: #a0a0a0 !important; font-family: 'Montserrat', sans-serif; font-size: 0.5rem;">Copyright © 2024 Jobgrader Innovations GmbH All rights reserved.</p>
		<p style="color: #a0a0a0 !important; font-family: 'Montserrat', sans-serif; font-size: 0.4rem;">SHA-256 ${sha}</p>
	</footer>
	</div>
	</body>
	</html>
	`;

	admin.firestore().collection('mail').add({
		to: "minter@blockchain-helix.com",
		message: {
			subject: 'Minting Error!',
			html: mailContent,
		},
	}).catch(e => {
		console.log("Failed to send the notifier mail!")
	})
}

const emailTemplateReferred = (points, callname?) => {
	const content = !!callname ? `<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="bff8ffa1-41a9-4aab-a2ea-52ac3767c6f4" data-mc-module-version="2019-10-22">
    <tbody>
        <tr>
            <td style="padding:18px 30px 18px 30px; line-height:40px; text-align:inherit;" height="100%" valign="top" role="module-content">
            <div>
                <div style="font-family: inherit; text-align: center"><span style="font-size: 40px; font-family: inherit; color: #54bf7b !important;">
                    Hi ${callname}!
                    </span>
                </div>
                <div></div>
            </div>
            </td>
        </tr>
    </tbody>
</table>
<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="2f94ef24-a0d9-4e6f-be94-d2d1257946b0" data-mc-module-version="2019-10-22">
    <tbody>
        <tr>
            <td style="padding:18px 50px 18px 50px; line-height:40px; text-align:inherit;" height="100%" valign="top" role="module-content">
                <div>
                    <div style="font-family: inherit; font-size: 40px; text-align: center; color: #000000 !important;">Congratulations! <br><br>Your referral code was processed. You receieve:</div>
                    <div style="font-family: inherit; text-align: inherit"><br></div>
                        <div style="font-family: inherit; text-align: center">
                        <span style="font-size: 40px; font-family: inherit; color: #54bf7b !important;">
                    ${points} points
                        </span>
                        </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>` : `<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="bff8ffa1-41a9-4aab-a2ea-52ac3767c6f4" data-mc-module-version="2019-10-22">
    <tbody>
        <tr>
            <td style="padding:18px 30px 18px 30px; line-height:40px; text-align:inherit;" height="100%" valign="top" role="module-content">
            <div>
                <div style="font-family: inherit; text-align: center"><span style="font-size: 40px; font-family: inherit; color: #54bf7b !important;">
                    Hi!
                    </span>
                </div>
                <div></div>
            </div>
            </td>
        </tr>
    </tbody>
</table>
<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="2f94ef24-a0d9-4e6f-be94-d2d1257946b0" data-mc-module-version="2019-10-22">
    <tbody>
        <tr>
            <td style="padding:18px 50px 18px 50px; line-height:40px; text-align:inherit;" height="100%" valign="top" role="module-content">
                <div>
                    <div style="font-family: inherit; font-size: 40px; text-align: center; color: #000000 !important;">Congratulations! <br><br>Your referral code was processed. You receieve:</div>
                    <div style="font-family: inherit; text-align: inherit"><br></div>
                        <div style="font-family: inherit; text-align: center">
                        <span style="font-size: 40px; font-family: inherit; color: #54bf7b !important;">
                    ${points} points
                        </span>
                        </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>`;
	const now = new Date();
	const sha = hash(content + now);
	const html = coreTemplate(content, sha);
	return html;
}

const emailTemplateReferrer = (points, callname?) => {
	const content = !!callname ? `<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="bff8ffa1-41a9-4aab-a2ea-52ac3767c6f4" data-mc-module-version="2019-10-22">
    <tbody>
        <tr>
            <td style="padding:18px 30px 18px 30px; line-height:40px; text-align:inherit;" height="100%" valign="top" role="module-content">
            <div>
                <div style="font-family: inherit; text-align: center"><span style="font-size: 40px; font-family: inherit; color: #54bf7b !important;">
                    Hi ${callname}!
                    </span>
                </div>
                <div></div>
            </div>
            </td>
        </tr>
    </tbody>
</table>
<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="2f94ef24-a0d9-4e6f-be94-d2d1257946b0" data-mc-module-version="2019-10-22">
    <tbody>
        <tr>
            <td style="padding:18px 50px 18px 50px; line-height:40px; text-align:inherit;" height="100%" valign="top" role="module-content">
            <div>
                <div style="font-family: inherit; font-size: 40px; text-align: center; color: #000000 !important;">Somebody used your referral code. You receieve:</div>
                <div style="font-family: inherit; text-align: inherit"><br></div>
                    <div style="font-family: inherit; text-align: center">
                    <span style="font-size: 40px; font-family: inherit; color: #54bf7b !important;">
                ${points} points
                    </span>
                    </div>
            </div>
            </td>
        </tr>
    </tbody>
</table>` : `<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="bff8ffa1-41a9-4aab-a2ea-52ac3767c6f4" data-mc-module-version="2019-10-22">
    <tbody>
        <tr>
            <td style="padding:18px 30px 18px 30px; line-height:40px; text-align:inherit;" height="100%" valign="top" role="module-content">
            <div>
                <div style="font-family: inherit; text-align: center"><span style="font-size: 40px; font-family: inherit; color: #54bf7b !important;">
                    Hi!
                    </span>
                </div>
                <div></div>
            </div>
            </td>
        </tr>
    </tbody>
</table>
<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="2f94ef24-a0d9-4e6f-be94-d2d1257946b0" data-mc-module-version="2019-10-22">
    <tbody>
        <tr>
            <td style="padding:18px 50px 18px 50px; line-height:40px; text-align:inherit;" height="100%" valign="top" role="module-content">
            <div>
                <div style="font-family: inherit; font-size: 40px; text-align: center; color: #000000 !important;">Somebody used your referral code. You receieve:</div>
                <div style="font-family: inherit; text-align: inherit"><br></div>
                    <div style="font-family: inherit; text-align: center">
                    <span style="font-size: 40px; font-family: inherit; color: #54bf7b !important;">
                ${points} points
                    </span>
                    </div>
            </div>
            </td>
        </tr>
    </tbody>
</table>`;
	const now = new Date();
	const sha = hash(content + now);
	const html = coreTemplate(content, sha);
	return html;
}

// Firebase Authentication middleware
export const authenticate = async (req, res, next) => {
	const authToken = req.headers.authorization;

	if (!authToken || !authToken.startsWith('Bearer ')) {
		return res.status(401).json({ error: 'Unauthorized' });
	}

	const token = authToken.split('Bearer ')[1];
	try {
		const decodedToken = await admin.auth().verifyIdToken(token);
		req.user = decodedToken;
		next();
	} catch (error) {
		// console.error('Error authenticating user:', error);
		return res.status(401).json({ error: 'Unauthorized' });
	}
};

// TODO @ansik: remove this once you have migrated everything to the new authenticate method
export const verifyToken = async (req): Promise<DecodedIdToken> => {
	const authHeader = req.headers.authorization;

	if (!authHeader || !authHeader.startsWith("Bearer ")) {
		return null;
	}

	const token = authHeader.split(" ")[1];

	try {
		const decodedToken = await admin.auth().verifyIdToken(token);
		return decodedToken;
	} catch (error) {
		return null;
	}
};

export const verifyApiKey = async (req): Promise<boolean> => {
	const authHeader = req.headers.authorization;

	if (!authHeader || !authHeader.startsWith("ApiKey ")) {
		return false;
	}

	const token = authHeader.split(" ")[1];

	try {
		const discord = (await admin.firestore().doc(`environment/discord`).get()).data();
		const keys = [discord.normalBotApiKey, discord.kycBotApiKey];

		return keys.includes(token);
	} catch (error) {
		return false;
	}
};

export const generateAuthToken = async (uid) => {
	const token = await admin.auth().createCustomToken(uid);
	return token;
};

export const sendPushNotification =
	async (title: string, body: string, userId: string, deviceId?: string) => {
		const userEntry = (await admin.firestore().doc(`users/${userId}`).get());

		try {
			const { firebaseTokens } = userEntry.data();

			if (!deviceId) {
				firebaseTokens.forEach((token) => {
					const message = {
						notification: { title, body },
						token: !!token.firebaseToken ? token.firebaseToken : token,
					};
					try {
						admin.messaging().send(message);
					} catch (error) {
						console.log(error);
					}
				});
			} else {
				const entry = firebaseTokens.find(f => f.deviceId == deviceId);
				if (entry) {
					const message = {
						notification: { title, body },
						token: entry.firebaseToken,
					};
					try {
						admin.messaging().send(message);
					} catch (error) {
						console.log(error);
					}
				} else {
					console.log(`Token not found for the user: ${userId}, deviceId: ${deviceId}`);
				}
			}

		} catch (error) {
			console.log(error);
		}

	};

export const sendEmailToReferrer = async (
	to: string,
	name?: string
): Promise<boolean> => {
	const environment = (await admin.firestore().doc(`environment/prod`).get()).data();
	return new Promise((resolve) => {
		admin.firestore().collection('mail').add({
			to,
			message: {
				subject: 'Jobgrader Referrals!',
				html: emailTemplateReferrer(environment.pointsToReferrer, name),
			},
		}).then(() => {
			console.log("Queued email for delivery!");
			resolve(true);
		}).catch(e => {
			resolve(false);
		})
	})

};

export const sendEmailToReferred = async (
	to: string,
	name?: string
): Promise<boolean> => {
	const environment = (await admin.firestore().doc(`environment/prod`).get()).data();
	return new Promise((resolve) => {
		admin.firestore().collection('mail').add({
			to,
			message: {
				subject: 'Jobgrader Referrals!',
				html: emailTemplateReferred(environment.pointsToReferred, name),
			},
		}).then(() => {
			console.log("Queued email for delivery!");
			resolve(true);
		}).catch(e => {
			resolve(false);
		})
	})

};

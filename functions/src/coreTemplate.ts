export const coreTemplate = (content, sha) => {
	return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
      
      <meta name="color-scheme" content="light dark">
      <meta name="color-scheme" content="light dark" />
      <meta name="supported-color-schemes" content="light dark">
      <meta name="supported-color-schemes" content="light dark" />

      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
      
      <style type="text/css">
      :root {
         color-scheme: light dark;
         supported-color-schemes: light dark;
         --colorececec: #ececec;
         --color252525: #D0D0D0;
         --colorffffff: #000000;
         --color000000: #FFFFFF;
         --colorff9c00: #ff9c00; 
         --color54bf7b: #A94484;
         --colorf5f8fd: #0A0702;
      }
      </style>
      
      <!--[if !mso]><!-->
      <meta http-equiv="X-UA-Compatible" content="IE=Edge">
      <!--<![endif]-->
      <!--[if (gte mso 9)|(IE)]>
      <xml>
         <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
         </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
      <!--[if (gte mso 9)|(IE)]>
      <style type="text/css">
         body {width: 600px;margin: 0 auto;}
         table {border-collapse: collapse;}
         table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
         img {-ms-interpolation-mode: bicubic;}
      </style>
      <![endif]-->
      <style type="text/css">
         body, p, div {
         font-family: "Jost", sans-serif;
         font-size: 14px;
         }
         body {
         color: #000000 !important;
         }
         body a {
         color: #000000 !important;
         text-decoration: none;
         }
         p { margin: 0; padding: 0; }
         table.wrapper {
         width:100% !important;
         table-layout: fixed;
         -webkit-font-smoothing: antialiased;
         -webkit-text-size-adjust: 100%;
         -moz-text-size-adjust: 100%;
         -ms-text-size-adjust: 100%;
         }
         img.max-width {
         max-width: 100% !important;
         }
         .column.of-2 {
         width: 50%;
         }
         .column.of-3 {
         width: 33.333%;
         }
         .column.of-4 {
         width: 25%;
         }
         ul ul ul ul  {
         list-style-type: disc !important;
         }
         ol ol {
         list-style-type: lower-roman !important;
         }
         ol ol ol {
         list-style-type: lower-latin !important;
         }
         ol ol ol ol {
         list-style-type: decimal !important;
         }
         @media screen and (max-width:480px) {
         .preheader .rightColumnContent,
         .footer .rightColumnContent {
         text-align: left !important;
         }
         .preheader .rightColumnContent div,
         .preheader .rightColumnContent span,
         .footer .rightColumnContent div,
         .footer .rightColumnContent span {
         text-align: left !important;
         }
         .preheader .rightColumnContent,
         .preheader .leftColumnContent {
         font-size: 80% !important;
         padding: 5px 0;
         }
         table.wrapper-mobile {
         width: 100% !important;
         table-layout: fixed;
         }
         img.max-width {
         height: auto !important;
         max-width: 100% !important;
         }
         a.bulletproof-button {
         display: block !important;
         width: auto !important;
         font-size: 80%;
         padding-left: 0 !important;
         padding-right: 0 !important;
         }
         .columns {
         width: 100% !important;
         }
         .column {
         display: block !important;
         width: 100% !important;
         padding-left: 0 !important;
         padding-right: 0 !important;
         margin-left: 0 !important;
         margin-right: 0 !important;
         }
         .social-icon-column {
         display: inline-block !important;
         }
         }
      </style>
      <!--user entered Head Start-->
      <link href="https://fonts.googleapis.com/css?family=Viga&display=swap" rel="stylesheet">
      <style>
         body {
            font-family: 'Viga', sans-serif;
            color:#000000 !important; 
            background-color:#D0D0D0 !important;
         }
      </style>
      <!--End Head user entered-->
   </head>
   <body>
      
         <center class="wrapper" data-link-color="#000000" data-body-style="font-size:14px; font-family:inherit; color:#000000 !important; background-color:#D0D0D0 !important;">
            <div class="webkit">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper">
                   <tr>
                      <td valign="top" width="100%">
                         <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                               <td width="100%">
                                  <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                     <tr>
                                        <td>
                                           <!--[if mso]>
                                           <center>
                                              <table>
                                                 <tr>
                                                    <td width="600">
                                                       <![endif]-->
                                                       <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width:100%; max-width:600px;" align="center">
                                                          <tr>
                                                             <td role="modules-container" style="padding:0px 0px 0px 0px; color:#000000 !important; text-align:left;" width="100%" align="left">
                                                                <table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%" style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
                                                                   <tr>
                                                                      <td role="module-content">
                                                                         <p></p>
                                                                      </td>
                                                                   </tr>
                                                                </table>
                                                                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns" style="padding:0px 0px 0px 0px;" data-distribution="1">
                                                                   <tbody>
                                                                      <tr role="module-content">
                                                                         <td height="100%" valign="top">
                                                                            <table width="580" style="width:580px; border-spacing:0; border-collapse:collapse; margin:0px 10px 0px 10px;" cellpadding="0" cellspacing="0" align="left" border="0" class="column column-0">
                                                                               <tbody>
                                                                                  <tr>
                                                                                     <td style="padding:0px;margin:0px;border-spacing:0;">
                                                                                        <table class="module" role="module" data-type="spacer" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="10cc50ce-3fd3-4f37-899b-a52a7ad0ccce">
                                                                                           <tbody>
                                                                                              <tr>
                                                                                                 <td style="padding:0px 0px 40px 0px;" role="module-content">
                                                                                                 </td>
                                                                                              </tr>
                                                                                           </tbody>
                                                                                        </table>
                                                                                        <table class="module" role="module" data-type="spacer" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="10cc50ce-3fd3-4f37-899b-a52a7ad0ccce.2">
                                                                                           <tbody>
                                                                                              <tr>
                                                                                                 <td style="padding:0px 0px 40px 0px;" role="module-content">
                                                                                                 </td>
                                                                                              </tr>
                                                                                           </tbody>
                                                                                        </table>
                                                                                        <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="f8665f9c-039e-4b86-a34d-9f6d5d439327">
                                                                                           <tbody>
                                                                                              <tr>
                                                                                                 <td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="center">
                                                                                                    <img class="max-width" border="0" style="display:block; color:#FFFFFF !important; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px;" width="" alt="" data-proportionally-constrained="false" data-responsive="false" src="https://dev-jobgrader.s3.eu-central-1.amazonaws.com/jobgrader-public/email-template-resources/header.png" height="">
                                                                                                 </td>
                                                                                              </tr>
                                                                                           </tbody>
                                                                                        </table>
                                                                                        <table class="module" role="module" data-type="spacer" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="10cc50ce-3fd3-4f37-899b-a52a7ad0ccce.1">
                                                                                           <tbody>
                                                                                              <tr>
                                                                                                 <td style="padding:0px 0px 30px 0px;" role="module-content">
                                                                                                 </td>
                                                                                              </tr>
                                                                                           </tbody>
                                                                                        </table>
                                                                                     </td>
                                                                                  </tr>
                                                                               </tbody>
                                                                            </table>
                                                                         </td>
                                                                      </tr>
                                                                   </tbody>
                                                                </table>
                                                                <content>
                                                                    ${content}
                                                                </content>
                                                                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns" style="padding:30px 0px 0px 0px;" data-distribution="1">
                                                                   <tbody>
                                                                      <tr role="module-content">
                                                                         <td height="100%" valign="top">
                                                                            <table width="600" style="width:600px; border-spacing:0; border-collapse:collapse; margin:0px 0px 0px 0px;" cellpadding="0" cellspacing="0" align="left" border="0" class="column column-0">
                                                                               <tbody>
                                                                                  <tr>
                                                                                     <td style="padding:0px;margin:0px;border-spacing:0;">
                                                                                        <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="ce6dd3be-5ed4-42d2-b304-55a58022cdf0">
                                                                                           <tbody>
                                                                                              <tr>
                                                                                                 <td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="center">
                                                                                                    <img class="max-width" border="0" style="display:block; color:#FFFFFF !important; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px; max-width:100% !important; width:100%; height:auto !important;" width="600" alt="" data-proportionally-constrained="true" data-responsive="true" src="https://dev-jobgrader.s3.eu-central-1.amazonaws.com/jobgrader-public/email-template-resources/lady.jpg">
                                                                                                 </td>
                                                                                              </tr>
                                                                                           </tbody>
                                                                                        </table>
                                                                                     </td>
                                                                                  </tr>
                                                                               </tbody>
                                                                            </table>
                                                                         </td>
                                                                      </tr>
                                                                   </tbody>
                                                                </table>
                                                                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns" style="padding:40px 30px 30px 30px;" data-distribution="1,1,1">
                                                                   <tbody>
                                                                      <tr role="module-content">
                                                                         <td height="100%" valign="top">
                                                                            <table width="166" style="width:166px; border-spacing:0; border-collapse:collapse; margin:0px 10px 0px 0px;" cellpadding="0" cellspacing="0" align="left" border="0" class="column column-0">
                                                                               <tbody>
                                                                                  <tr>
                                                                                     <td style="padding:0px;margin:0px;border-spacing:0;">
                                                                                        <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="35f4b6e7-fc49-4a6f-a23c-e84ad33abca4">
                                                                                           <tbody>
                                                                                              <tr>
                                                                                                 <td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="center">
                                                                                                    <!-- <img class="max-width" border="0" style="display:block; color:#FFFFFF !important; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px;" width="80" alt="" data-proportionally-constrained="true" data-responsive="false" src="http://cdn.mcauto-images-production.sendgrid.net/954c252fedab403f/0394b217-16c4-49ae-b696-561adcd513aa/119x119.png" height="80"> -->
                                                                                                    <div class="max-width" style="display:block;width:80px;" alt="" data-proportionally-constrained="true">
                                                                                                        <img style="width:80px;" src="https://dev-jobgrader.s3.eu-central-1.amazonaws.com/jobgrader-public/email-template-resources/telegram.png" alt="">
                                                                                                    </div>
                                                                                                 </td>
                                                                                              </tr>
                                                                                           </tbody>
                                                                                        </table>
                                                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="4f3e6dad-4d49-49b4-b842-97c93e43616f" data-mc-module-version="2019-10-22">
                                                                                           <tbody>
                                                                                              <tr>
                                                                                                 <td style="padding:18px 0px 18px 0px; line-height:22px; text-align:inherit;" height="100%" valign="top" role="module-content">
                                                                                                    <div>
                                                                                                       <div style="font-family: inherit; text-align: inherit"><span style="font-size: 14px; color: #000000 !important;">Join our Telegram channel! Stay updated and connect with like-minded people.</span></div>
                                                                                                       <div style="font-family: inherit; text-align: inherit"><br></div>
                                                                                                       <div style="font-family: inherit; text-align: inherit"><a target="_blank" href="https://t.me/jobgrader"><span style="font-size: 14px; color: #000000 !important;"><u>Learn More</u></span></a></div>
                                                                                                       <div></div>
                                                                                                    </div>
                                                                                                 </td>
                                                                                              </tr>
                                                                                           </tbody>
                                                                                        </table>
                                                                                     </td>
                                                                                  </tr>
                                                                               </tbody>
                                                                            </table>
                                                                            <table width="166" style="width:166px; border-spacing:0; border-collapse:collapse; margin:0px 10px 0px 10px;" cellpadding="0" cellspacing="0" align="left" border="0" class="column column-1">
                                                                               <tbody>
                                                                                  <tr>
                                                                                     <td style="padding:0px;margin:0px;border-spacing:0;">
                                                                                        <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="0cb2f52e-e1c0-4b42-a114-04aa36fe57f5">
                                                                                           <tbody>
                                                                                              <tr>
                                                                                                 <td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="center">
                                                                                                    <!-- <img class="max-width" border="0" style="display:block; color:#FFFFFF !important; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px;" width="80" alt="" data-proportionally-constrained="true" data-responsive="false" src="http://cdn.mcauto-images-production.sendgrid.net/954c252fedab403f/461a0641-b2b7-459c-ab49-ea560fc221f7/119x119.png" height="80"> -->
                                                                                                    <div class="max-width" style="display:block;width:80px;" alt="" data-proportionally-constrained="true">
                                                                                                       <img style="width:80px;" src="https://dev-jobgrader.s3.eu-central-1.amazonaws.com/jobgrader-public/email-template-resources/jobgrader.png" alt="">
                                                                                                    </div>
                                                                                                 </td>
                                                                                              </tr>
                                                                                           </tbody>
                                                                                        </table>
                                                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="9bf90608-97e0-467e-a709-f45d87b0451b" data-mc-module-version="2019-10-22">
                                                                                           <tbody>
                                                                                              <tr>
                                                                                                 <td style="padding:18px 0px 18px 0px; line-height:22px; text-align:inherit;" height="100%" valign="top" role="module-content">
                                                                                                    <div>
                                                                                                       <div style="font-family: inherit; text-align: inherit"><span style="color: #000000 !important; font-family: inherit; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; white-space-collapse: preserve; text-wrap: wrap; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline">Unlock your potential! Qualify for higher paying jobs at our assessment centre.</span></div>
                                                                                                       <div style="font-family: inherit; text-align: inherit"><br></div>
                                                                                                       <div style="font-family: inherit; text-align: inherit"><a target="_blank" href="https://learning.jobgrader.app/"><span style="font-size: 14px; color: #000000 !important;"><u>Learn More</u></span></a></div>
                                                                                                       <div></div>
                                                                                                    </div>
                                                                                                 </td>
                                                                                              </tr>
                                                                                           </tbody>
                                                                                        </table>
                                                                                     </td>
                                                                                  </tr>
                                                                               </tbody>
                                                                            </table>
                                                                            <table width="166" style="width:166px; border-spacing:0; border-collapse:collapse; margin:0px 0px 0px 10px;" cellpadding="0" cellspacing="0" align="left" border="0" class="column column-2">
                                                                               <tbody>
                                                                                  <tr>
                                                                                     <td style="padding:0px;margin:0px;border-spacing:0;">
                                                                                        <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="231c1abd-75e6-4f22-a697-c5f3819b2b07">
                                                                                           <tbody>
                                                                                              <tr>
                                                                                                 <td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="center">
                                                                                                    <!-- <img class="max-width" border="0" style="display:block; color:#FFFFFF !important; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px;" width="80" alt="" data-proportionally-constrained="true" data-responsive="false" src="http://cdn.mcauto-images-production.sendgrid.net/954c252fedab403f/61f17ba7-b7af-4276-8e61-2501e525e4c3/119x119.png" height="80"> -->
                                                                                                    <div class="max-width" style="display:block;width:80px;" alt="" data-proportionally-constrained="true">
                                                                                                       <img style="width:80px;" src="https://dev-jobgrader.s3.eu-central-1.amazonaws.com/jobgrader-public/email-template-resources/discord.png" alt="">
                                                                                                    </div>
                                                                                                 </td>
                                                                                              </tr>
                                                                                           </tbody>
                                                                                        </table>
                                                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="e82d5e62-b94c-42bb-a289-4515ec9ecc85" data-mc-module-version="2019-10-22">
                                                                                           <tbody>
                                                                                              <tr>
                                                                                                 <td style="padding:18px 0px 18px 0px; line-height:22px; text-align:inherit;" height="100%" valign="top" role="module-content">
                                                                                                    <div>
                                                                                                       <div style="font-family: inherit; text-align: inherit"><span style="font-size: 14px; color: #000000 !important;">Join our Discord community! Stay active and never miss the latest job announcements.</span></div>
                                                                                                       <div style="font-family: inherit; text-align: inherit"><br></div>
                                                                                                       <div style="font-family: inherit; text-align: inherit"><a target="_blank" href="https://discord.gg/A4sea9GVZp"><span style="font-size: 14px; color: #000000 !important;"><u>Learn More</u></span></a></div>
                                                                                                       <div></div>
                                                                                                    </div>
                                                                                                 </td>
                                                                                              </tr>
                                                                                           </tbody>
                                                                                        </table>
                                                                                     </td>
                                                                                  </tr>
                                                                               </tbody>
                                                                            </table>
                                                                         </td>
                                                                      </tr>
                                                                   </tbody>
                                                                </table>
                                                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="30d9a68c-ce13-4754-a845-6c3dc22721ee" data-mc-module-version="2019-10-22">
                                                                   <tbody>
                                                                      <tr>
                                                                         <td style="padding:40px 40px 40px 40px; line-height:22px; text-align:inherit; background-color:#ff9c00 !important;" height="100%" valign="top" role="module-content">
                                                                            <div>
                                                                               <div style="font-family: inherit; text-align: center"><span style="color: #000000 !important; font-size: 16px">Need more help figuring things out? <br>Our support team on Discord is here to help!</span></div>
                                                                               <div style="font-family: inherit; text-align: center"><br></div>
                                                                               <div style="font-family: inherit; text-align: center"><a target="_blank" href="https://discord.gg/A4sea9GVZp"><span style="color: #000000 !important; font-size: 16px"><u>Help Center</u></span></a></div>
                                                                               <div></div>
                                                                            </div>
                                                                         </td>
                                                                      </tr>
                                                                   </tbody>
                                                                </table>
                                                                <table border="0" cellpadding="0" cellspacing="0" class="module" data-role="module-button" data-type="button" role="module" style="table-layout:fixed;" width="100%" data-muid="188c3d22-338c-4a35-a298-a7d3957f579d">
                                                                   <tbody>
                                                                      <tr>
                                                                         <td align="center" class="outer-td" style="padding:0px 0px 20px 0px;">
                                                                            <table border="0" cellpadding="0" cellspacing="0" class="wrapper-mobile" style="text-align:center;padding-top:10px;">
                                                                               <tbody>
                                                                                  <tr>
                                                                                     <td align="center" class="inner-td" style="border-radius:6px; font-size:16px; text-align:center; background-color:inherit;"><a target="_blank" href="https://jobgrader.app/ai" style="background-color:#ffffff !important; border:1px solid #0A0702; border-color:#0A0702 !important; border-radius:25px; border-width:1px; color:#000000 !important; display:inline-block; font-size:10px; font-weight:normal; letter-spacing:0px; line-height:normal; padding:5px 18px 5px 18px; text-align:center; text-decoration:none; border-style:solid; font-family:helvetica,sans-serif;" target="_blank">♥ POWERED BY Jobgrader</a></td>
                                                                                  </tr>
                                                                               </tbody>
                                                                            </table>
                                                                         </td>
                                                                      </tr>
                                                                   </tbody>
                                                                </table>
                                                             </td>
                                                          </tr>
                                                       </table>
                                                       <!--[if mso]>
                                                    </td>
                                                 </tr>
                                              </table>
                                           </center>
                                           <![endif]-->
                                        </td>
                                     </tr>
                                  </table>
                               </td>
                            </tr>
                         </table>
                      </td>
                   </tr>
                </table>
             </div>
            <footer style="background-color: #ececec !important; width: 100%; padding-top: 0.5%; padding-bottom: 0.5%;">
                <p style="color: #a0a0a0 !important; font-family: inherit; font-size: 0.9rem;">
                   <span ><a style="color: #a0a0a0 !important; font-family: inherit;" href="https://jobgrader.app/privacy-policy">Privacy Policy</a></span>
                   <span><a style="color: #a0a0a0 !important; font-family: inherit;" href="https://jobgrader.app/imprint">Imprint</a></span>
                   <span><a style="color: #a0a0a0 !important; font-family: inherit;" href="https://jobgrader.app/">Contact</a></span>
                </p>
                <p style="color: #a0a0a0 !important; font-family: inherit; font-size: 0.7rem;">Copyright © 2024 Jobgrader Innovations GmbH All rights reserved.</p>
                <p style="color: #a0a0a0 !important; font-family: inherit; font-size: 0.6rem;">SHA-256 ${sha}</p>
             </footer>

         </center>
         
      
      
   </body>
</html>`;
}


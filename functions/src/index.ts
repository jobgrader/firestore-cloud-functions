/* tslint:disable */

import * as admin from "firebase-admin";
import * as functions from "firebase-functions/v2";
import { decryptBody, generateBlueTokens, symmetricDecrypt } from "./decrypt";
import { randomString, processedTimestamp, convertTimestamp, mapTrustData, TelegramUser, validateTelegramHash, checkSolutionEquality } from "./utility";
import { authenticate, sendEmailToReferrer, sendEmailToReferred, verifyToken, sendPushNotification, verifyApiKey, mintAssessmentNft, addTextToImage, fetchEpnNfts } from "./services";
import { firebaseConfig } from "./serviceAccount";
import cors from 'cors';
import axios from "axios";


const corsOptions = {
  origin: true,
  methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
  allowedHeaders: ['Authorization', 'Content-Type'],
};

const exceptionOptions = {
  origin: true,
  methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
  allowedHeaders: ['Content-Type'],
};

admin.initializeApp({
  credential: admin.credential.cert(firebaseConfig as admin.ServiceAccount)
})

const opts: functions.https.HttpsOptions = {
  cors: true,
  // [
  //   "http://localhost:8100",
  //   "http://localhost:8101",
  //   "jobgrader://jobgrader.com",
  //   "https://pwa-test.jobgrader.app",
  //   "https://jobgrader.app",
  //   "https://jobgrader.com",
  // ],
  region: "europe-west1"
};

exports.twitterwebhook = functions.https.onRequest(opts, async (request, response) => {

  console.log(request.params);
  console.log(request.body);
  response.status(200).send({
    success: true,
  });

});

exports.twitteroauth = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();
  const twitter = (await admin.firestore().doc(`environment/twitter`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const apiKeyValue = twitter.apiKeyValue;
        const apiSecretValue = twitter.apiSecretValue;
        // const bearerTokenValue = twitter.bearerTokenValue;

        const generateAuthorizationHeader = (apiKey, apiSecret) => {
          const encodedApiKey = Buffer.from(`${apiKey}:${apiSecret}`).toString('base64');
          return `Basic ${encodedApiKey}`;
        }

        const requestToken = async () => {
          const url = 'https://api.twitter.com/oauth2/token';
          const headers = {
            Authorization: generateAuthorizationHeader(apiKeyValue, apiSecretValue),
            'Content-Type': 'application/x-www-form-urlencoded',
          };
          const data = {
            grant_type: 'client_credentials',
          };

          try {
            const response = await axios.post(url, data, { headers });
            return response.data.access_token;
          } catch (error) {
            console.error('Error requesting token:', error);
            throw error;
          }
        }

        try {
          const accessToken = await requestToken();
          const authorizationUrl = `https://twitter.com/i/flow/login?oauth_token=${accessToken}`;

          response.status(200).send({
            success: true,
            url: authorizationUrl
          });

        } catch (error) {

          response.status(200).send({
            success: false,
          });

        }

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })


});

exports.auth0management = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();
  const { domain, bearerToken, roleId } = (await admin.firestore().doc(`environment/auth0`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response

            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const body: string = request.body as string;

        let cleaned: string;

        try {
          cleaned = decodeURIComponent(body);
        } catch (error) {
          cleaned = body;
        }



        let query: any;

        try {
          query = await decryptBody(cleaned);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError
            })
        }

        let parsed: any;

        try {
          parsed = JSON.parse(query);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError
            })
        }

        const { userId } = parsed;

        if (!userId) {
          response
            .status(401).send({
              success: false
            });
        } else {

          const req = await axios.post(`https://${domain}/api/v2/users/${userId}/roles`, [{
            "role": roleId
          }], {
            headers: {
              "Authorization": `Bearer ${bearerToken}`,
              "Content-Type": "application/json"
            }
          });

          if (req.data) {
            console.log(req.data);
          }

          response.status(200).send({
            success: true
          })

        }

      })
    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }
  }

  )

})

exports.obtainusergroups = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {



      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const body: string = request.body as string;

        let cleaned: string;

        try {
          cleaned = decodeURIComponent(body);
        } catch (error) {
          cleaned = body;
        }

        let query: any;

        try {
          query = await decryptBody(cleaned);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError
            })
        }

        let parsed: any;

        try {
          parsed = JSON.parse(query);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError
            })
        }

        const { email, nationalities, languageProficiencies, deviceId, firebaseToken, gender } = parsed;
        const userId = tokenVerificationResponse.uid;

        const existingContent = await admin.firestore().doc(`users/${userId}`).get();

        let firebaseTokens = [];

        if (!!firebaseToken && !!deviceId) {

          console.log({ firebaseToken });

          if (existingContent.exists) {
            firebaseTokens = existingContent.data().firebaseTokens;

            const check = firebaseTokens.find(k => k.deviceId == deviceId);
            if (check) {
              firebaseTokens.find(k => k.deviceId == deviceId).firebaseToken = firebaseToken;
            } else {
              firebaseTokens.push({ deviceId, firebaseToken });
            }
          } else {
            firebaseTokens.push({ deviceId, firebaseToken });

            const message = {
              notification: { title: "", body: "🔥We are excited to welcome you as our very first app user tester. Discover, enjoy and share your feedback. Your participation will be rewarded with 400 points!" },
              token: firebaseToken,
            };

            console.log({ message });

            try {
              admin.messaging().send(message);
            } catch (error) {
              console.log(error);
            }

          }

        }

        let groups = !!existingContent.exists ? (!!existingContent.data().groups ? existingContent.data().groups : []) : [];

        console.log(groups);

        if (!groups) {
          var groupCheck = null;
        } else {
          try {
            groupCheck = groups.find(k => k == "default");
          } catch (error) {
            groupCheck = null;
          }

        }

        if (!groupCheck) {
          groups = ["default"];
        }

        await admin.firestore().doc(`users/${userId}`).set({
          email,
          gender,
          nationalities,
          languageProficiencies,
          firebaseTokens,
          groups,
          points: 0
        })

        response.status(200).send({
          success: true,
          groups
        })

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })


});

exports.obtainuserdata = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {



      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response

            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const userId = tokenVerificationResponse.uid;

        const doc = `users/${userId}`;
        const existingData = await admin.firestore().doc(doc).get();

        if (existingData.exists) {
          const ed = existingData.data();
          const { gender, nationalities, languageProficiencies, points, groups } = ed;
          response
            .status(200).send({
              success: true,
              gender,
              nationalities: (nationalities == "") ? [] : nationalities,
              languageProficiencies: (languageProficiencies == "") ? [] : languageProficiencies,
              groups: !!groups ? groups : ["default"],
              points
            })

        } else {
          response
            .status(200).send({
              success: true,
              gender: null,
              nationalities: [],
              languageProficiencies: [],
              groups: ["default"],
              points: 0
            })
        }

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })


});

exports.manageuserentry = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const body: string = request.body as string;

        let cleaned: string;

        try {
          cleaned = decodeURIComponent(body);
        } catch (error) {
          cleaned = body;
        }

        let query;

        try {
          query = await decryptBody(cleaned);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        let parsed: any;

        try {
          parsed = JSON.parse(query);
          console.log(parsed);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        const { userGroups, usedReferralCode } = parsed;
        const userId = tokenVerificationResponse.uid;


        console.log(userGroups);
        console.log(usedReferralCode);

        const collection = `referral_codes`;
        const documents = await admin.firestore().collection(collection).get();

        const email = (await admin.firestore().doc(`users/${userId}`).get()).data().email;

        let referrer = null;
        let payment = "0 points";

        for (const document of documents.docs) {
          const referrals = document.data().referrals;
          const referralCodeArray = Array.from(referrals, (rrr: any) => rrr.code);
          if (referralCodeArray.includes(usedReferralCode)) {
            referrer = document.data();

            const referralCategory = referrals.find(r => r.code == usedReferralCode).category;
            const maxReferredEmails = (await admin.firestore().doc(`referral_programs/${referralCategory}`).get()).data().maxReferredEmails;

            if (referrer) {
              if (referrer.referredEmails.length < maxReferredEmails) {
                payment = environment.pointsToReferred + " points";
                let newReferredEmail = referrer.referredEmails.concat([email]);
                await admin.firestore().doc(`${collection}/${document.id}`).update({
                  email: referrer.email,
                  referrals: referrer.referrals,
                  maxReferredEmails: maxReferredEmails,
                  referredEmails: newReferredEmail,
                  usedReferralCode: usedReferralCode
                });

                const basicDataUrl = `${process.env.MW_API_URL}/user/basicdata/getbyuserid/${userId}`;
                const trustUrl = `${process.env.MW_API_URL}/api/v1/user/${userId}/trust`;
                let firstname = null;

                try {
                  const blueToken = await generateBlueTokens();
                  const basicDataResponse = await axios.get(basicDataUrl, {
                    headers: {
                      "x-api-key": blueToken.encrypted,
                      Authorization: request.headers.authorization
                    }
                  });
                  const trustResponse = await axios.get(trustUrl, {
                    headers: {
                      "x-api-key": blueToken.encrypted,
                      Authorization: request.headers.authorization,
                      "Content-Type": "application/json"
                    }
                  });

                  const basicData = JSON.parse(symmetricDecrypt(basicDataResponse.data, blueToken.key));
                  const trust = JSON.parse(symmetricDecrypt(trustResponse.data, blueToken.key));

                  const user = Object.assign({}, basicData, mapTrustData(trust));
                  console.log({ user });

                  firstname = (user.firstnameStatus == 1) ? user.firstname : null;
                } catch (e) {
                  console.log(e);
                }

                await sendEmailToReferred(email, firstname);
                await sendEmailToReferrer(referrer.email, firstname);

                await sendPushNotification(environment.referralPushNotificationTitle, environment.referredPushNotificationBody + " You receive " + environment.pointsToReferred + " points", userId);
                await sendPushNotification(environment.referralPushNotificationTitle, environment.referrerPushNotificationBody + " You receive " + environment.pointsToReferrer + " points", document.id);

                const referrerData = await admin.firestore().doc(`users/${document.id}`).get();
                const referredData = await admin.firestore().doc(`users/${userId}`).get();

                if (referredData.exists) {
                  await admin.firestore().doc(`users/${document.id}`).update({
                    firebaseTokens: !!referredData.data().firebaseTokens ? referredData.data().firebaseTokens : [],
                    gender: !!referredData.data().gender ? referredData.data().gender : "",
                    languageProficiencies: !!referredData.data().languageProficiencies ? referredData.data().languageProficiencies : [],
                    nationalities: !!referredData.data().nationalities ? referredData.data().nationalities : [],
                    groups: !!referredData.data().groups ? referredData.data().groups : ["default"],
                    points: !!referredData.data().points ? referredData.data().points + environment.pointsToReferred : environment.pointsToReferred,
                  })
                }

                if (referrerData.exists) {
                  await admin.firestore().doc(`users/${userId}`).update({
                    firebaseTokens: !!referrerData.data().firebaseTokens ? referrerData.data().firebaseTokens : [],
                    gender: !!referrerData.data().gender ? referrerData.data().gender : "",
                    languageProficiencies: !!referrerData.data().languageProficiencies ? referrerData.data().languageProficiencies : [],
                    nationalities: !!referrerData.data().nationalities ? referrerData.data().nationalities : [],
                    groups: !!referrerData.data().groups ? referrerData.data().groups : ["default"],
                    points: !!referrerData.data().points ? referrerData.data().points + environment.pointsToReferrer : environment.pointsToReferrer,
                  })
                }

              }
            }
          }
        }

        response
          .status(200).send({
            success: true,
            payment
          })

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })

});

exports.fetchreferralprograms = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {
    try {
      // Authenticate user
      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const userId = tokenVerificationResponse.uid;

        const data = [];

        const docR = `referral_codes/${userId}`;
        const referralCodes = (await admin.firestore().doc(docR).get()).data();

        const categories = await admin.firestore().collection(`referral_programs`).get();

        for (const element of categories.docs) {
          const ele = element.data();

          data.push({
            active: (ele.active && (new Date(processedTimestamp(ele.expiryDate)) > new Date())),
            referralCategory: element.id,
            title: ele.title,
            description: ele.description,
            maxReferredEmails: ele.maxReferredEmails,
            expiryDate: ele.expiryDate,
            generatedReferralCode: !!referralCodes ? (!!referralCodes.referrals ? (!!referralCodes.referrals.find(ue => ue.category == element.id) ? referralCodes.referrals.find(ue => ue.category == element.id).code : null) : null) : null
          })
        }

        response.status(200).send({
          success: true,
          data
        });

      });

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }
  });
});

exports.generatereferralcode = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const body: string = request.body as string;

        let cleaned: string;

        try {
          cleaned = decodeURIComponent(body);
        } catch (error) {
          cleaned = body;
        }

        let query;

        try {
          query = await decryptBody(cleaned);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        let parsed: any;

        try {
          parsed = JSON.parse(query);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        let userId: string = tokenVerificationResponse.uid;

        const referralCategory: string = parsed.referralCategory ?
          parsed.referralCategory as string : "";

        const generatedReferralCode: string = randomString(6).toUpperCase();

        const referralDoc = await admin.firestore().doc(`referral_codes/${userId}`).get();
        const userDoc = await admin.firestore().doc(`users/${userId}`).get();

        const email = userDoc.data().email;

        const referredEmails: Array<string> = (referralDoc.exists) ? referralDoc.data().referredEmails : [];

        let maxReferredEmails = (await admin.firestore().doc(`referral_programs/${referralCategory}`).get()).data().maxReferredEmails;

        referredEmails.push(email);

        let referrals = [];

        if (referralDoc.exists) {
          const categoryCheck = (referralDoc.data().referrals).find(c => c.category == referralCategory);

          if (!categoryCheck) {

            referrals = referralDoc.data().referrals.concat([{
              category: referralCategory,
              code: generatedReferralCode
            }]);

            await admin.firestore().doc(`referral_codes/${userId}`).set({
              email,
              referrals,
              referredEmails,
              maxReferredEmails,
            });

          }
        } else {

          await admin.firestore().doc(`referral_codes/${userId}`).set({
            email,
            referrals: [{
              category: referralCategory,
              code: generatedReferralCode
            }],
            referredEmails: [],
            maxReferredEmails,
          });

        }

        response
          .status(200).send({
            success: true,
            generatedReferralCode,
          });
        // }

      })



    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })


});

exports.checkreferralcodevalidity = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(exceptionOptions)(request, response, async () => {

    try {

      const body: string = request.body as string;

      let cleaned: string;

      try {
        cleaned = decodeURIComponent(body);
      } catch (error) {
        cleaned = body;
      }

      let query: any;

      try {
        query = await decryptBody(cleaned);
      } catch (error) {
        response
          .status(404).send({
            success: false,
            error: environment.decryptionError
          })
      }

      let parsed: any;

      try {
        parsed = JSON.parse(query);
      } catch (error) {
        response

          .status(404).send({
            success: false,
            error: environment.decryptionError
          })
      }

      const { code } = parsed;

      console.log({ code });

      const collection = `referral_codes`;
      const documents = await admin.firestore().collection(collection).get();

      let referrerUser = null;

      for (const doc of documents.docs) {
        const referralCodeArray = Array.from(doc.data().referrals, (rrr: any) => rrr.code);

        console.log({ referralCodeArray });

        if (referralCodeArray.includes(code)) {

          const category = doc.data().referrals.find(d => d.code == code).category;
          console.log({ category });
          const now = new Date();

          const referralCategories = (await admin.firestore().doc(`referral_programs/${category}`).get()).data();

          if ((new Date(processedTimestamp(referralCategories.expiryDate)) > now) && (doc.data().referredEmails.length < referralCategories.maxReferredEmails)) {
            referrerUser = doc.id;
            console.log("Inside Loop: ", { referrerUser });
          }

        }
      }

      if (referrerUser) {
        response
          .status(200).send({
            success: true,
          })
      } else {
        response
          .status(200).send({
            success: false,
            error: environment.codeExpired
          })
      }

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })


});


exports.returnpoints = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();
  const { pointsToReferred, pointsToReferrer } = environment;

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        response
          .status(200).send({
            success: true,
            pointsToReferred,
            pointsToReferrer
          });

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })


});

exports.fetchsocialmediatasks = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const collection = await admin.firestore().collection(`social_media_tasks`).get();

        let data = [];

        for (const document of collection.docs) {
          const { active, earnings, earningsToken, language, shareContent, targetApp, targetChannel, taskCode, taskDescription, inviteUrl } = document.data();

          data.push({
            taskId: document.id,
            active, earnings, earningsToken, language, shareContent, targetApp, targetChannel, taskCode, taskDescription, inviteUrl
          })
        }

        response
          .status(200).send({
            success: true,
            data
          });

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })


});

exports.fetchsocialuserstatus = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const userId = tokenVerificationResponse.uid;

        const collection = await admin.firestore().collection(`social_user_status`).get();

        let data = [];

        for (const doc of collection.docs) {
          if (doc.data().userId == userId) {
            const { earnings, earningsToken, status, taskId, timestamp, userId } = doc.data();
            data.push(
              { earnings, earningsToken, status, taskId, timestamp, userId }
            );
          }
        }

        response
          .status(200).send({
            success: true,
            data
          })


      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })


});

exports.updatesocialuserstatus = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const body: string = request.body as string;

        let cleaned: string;

        try {
          cleaned = decodeURIComponent(body);
        } catch (error) {
          cleaned = body;
        }

        let query;

        try {
          query = await decryptBody(cleaned);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        let parsed: any;

        try {
          parsed = JSON.parse(query);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        const { taskId, status, timestamp, earnings, earningsToken } = parsed;
        const userId = tokenVerificationResponse.uid;

        await admin.firestore().collection(`social_user_status`).add({
          userId,
          taskId,
          status,
          timestamp: convertTimestamp(timestamp),
          earnings,
          earningsToken
        });

        response
          .status(200).send({
            success: true,
          })

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })


});

exports.deleteuserentry = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const userId = tokenVerificationResponse.uid;

        const collections = ['users', 'referral_codes'];

        for (const collection of collections) {
          try {
            await admin.firestore().doc(`${collection}/${userId}`).delete();
          } catch (error) {
            console.log(error);
          }
        }

        response
          .status(200).send({
            success: true,
          })

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })


});

exports.updatesocialaccounts = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const body: string = request.body as string;

        let cleaned: string;

        try {
          cleaned = decodeURIComponent(body);
        } catch (error) {
          cleaned = body;
        }

        let query;

        try {
          query = await decryptBody(cleaned);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        let parsed: any;

        try {
          parsed = JSON.parse(query);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        const { discordId, facebookId, linkedinId, telegramId, twitterId, instagramId } = parsed;
        const userId = tokenVerificationResponse.uid;

        let accountChecker = null;

        const collection = await admin.firestore().collection(`social_accounts`).get();

        for (const doc of collection.docs) {
          if (!!discordId) {
            if (doc.data().discordId == discordId) {
              accountChecker = doc.id;
            }
          }
          if (!!facebookId) {
            if (doc.data().facebookId == facebookId) {
              accountChecker = doc.id;
            }
          }
          if (!!linkedinId) {
            if (doc.data().linkedinId == linkedinId) {
              accountChecker = doc.id;
            }
          }
          if (!!telegramId) {
            if (doc.data().telegramId == telegramId) {
              accountChecker = doc.id;
            }
          }
          if (!!twitterId) {
            if (doc.data().twitterId == twitterId) {
              accountChecker = doc.id;
            }
          }
          if (!!instagramId) {
            if (doc.data().instagramId == instagramId) {
              accountChecker = doc.id;
            }
          }

        }

        if (!accountChecker) {

          const content = await admin.firestore().doc(`social_accounts/${userId}`).get();

          if (!content.exists) {
            await admin.firestore().doc(`social_accounts/${userId}`).create({
              discordId: !!discordId ? discordId : "",
              facebookId: !!facebookId ? facebookId : "",
              linkedinId: !!linkedinId ? linkedinId : "",
              telegramId: !!telegramId ? telegramId : "",
              twitterId: !!twitterId ? twitterId : "",
              instagramId: !!instagramId ? instagramId : ""
            })
          } else {
            await admin.firestore().doc(`social_accounts/${userId}`).update({
              discordId: !!discordId ? discordId : "",
              facebookId: !!facebookId ? facebookId : "",
              linkedinId: !!linkedinId ? linkedinId : "",
              telegramId: !!telegramId ? telegramId : "",
              twitterId: !!twitterId ? twitterId : "",
              instagramId: !!instagramId ? instagramId : ""
            })
          }

          response
            .status(200).send({
              success: true,
            })

        } else {

          response
            .status(200).send({
              success: false,
              error: "Record already exists"
            })

        }

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })


});

exports.fetchsocialaccounts = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const userId = tokenVerificationResponse.uid;

        const content = await admin.firestore().doc(`social_accounts/${userId}`).get();

        if (!content.exists) {
          response
            .status(200).send({
              success: false,
              error: "User record does not exist"
            })

        } else {

          const { discordId, facebookId, linkedinId, telegramId, twitterId, instagramId } = content.data();

          response
            .status(200).send({
              success: true,
              data: { discordId, facebookId, linkedinId, telegramId, twitterId, instagramId }
            })
        }

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })


});

exports.updatefirebasetoken = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const body: string = request.body as string;

        let cleaned: string;

        try {
          cleaned = decodeURIComponent(body);
        } catch (error) {
          cleaned = body;
        }

        let query;

        try {
          query = await decryptBody(cleaned);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        let parsed: any;

        try {
          parsed = JSON.parse(query);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        const userId = tokenVerificationResponse.uid;

        const { deviceId, firebaseToken } = parsed;

        const content = await admin.firestore().doc(`users/${userId}`).get();

        if (!content.exists) {

          const basicDataUrl = `${process.env.MW_API_URL}/user/basicdata/getbyuserid/${userId}`;

          try {
            const blueToken = await generateBlueTokens();
            const basicDataResponse = await axios.get(basicDataUrl, {
              headers: {
                "x-api-key": blueToken.encrypted,
                Authorization: request.headers.authorization
              }
            });

            const basicData = JSON.parse(symmetricDecrypt(basicDataResponse.data, blueToken.key));

            await admin.firestore().doc(`users/${userId}`).create({
              firebaseTokens: [{ deviceId, firebaseToken }],
              gender: "",
              nationalities: [],
              languageProficiencies: [],
              email: basicData.email,
              groups: ["default"],
              points: 0
            })

            response
              .status(200).send({
                success: true
              })
          } catch (error) {

            response
              .status(200).send({
                success: false,
                error: "Failed to add a user record"
              })

          }

        } else {

          const firebaseTokens = !!content.data().firebaseTokens ? content.data().firebaseTokens : [];

          if (!!deviceId && !!firebaseToken) {

            const check = firebaseTokens.find(k => k.deviceId == deviceId);

            if (check) {
              firebaseTokens.find(k => k.deviceId == deviceId).firebaseToken = firebaseToken;
            } else {
              firebaseTokens.push({ deviceId, firebaseToken });
            }

            await admin.firestore().doc(`users/${userId}`).update({
              firebaseTokens: firebaseTokens
            })

            response
              .status(200).send({
                success: true
              })
          } else {
            response
              .status(200).send({
                success: false,
                error: "Missing deviceId or firebaseToken"
              })
          }

        }

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })


});

exports.telegramwebhook = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();
  const telegram = (await admin.firestore().doc(`environment/telegram`).get()).data();

  try {

    const url = new URL(telegram.redirectUri + request.url);
    const hashValue = url.hash.split("#tgAuthResult=")[1];
    const buffer = Buffer.from(hashValue, 'base64');
    const decodedResponse = buffer.toString('utf-8');
    const userData: TelegramUser = JSON.parse(decodedResponse);

    validateTelegramHash(userData, telegram.botToken)




  } catch (error) {
    console.error('Error in function:', error);
    response.status(500).json({ error: environment.serverError });
  }

});

exports.telegramdatachecker = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  try {

    await authenticate(request, response, async () => {

      const tokenVerificationResponse = await verifyToken(request);

      if (!tokenVerificationResponse) {
        response
          .status(401).send({
            success: false,
            error: environment.tokenError,
          });
      }

      const body: string = request.body as string;

      let cleaned: string;

      try {
        cleaned = decodeURIComponent(body);
      } catch (error) {
        cleaned = body;
      }

      let query;

      try {
        query = await decryptBody(cleaned);
      } catch (error) {
        response

          .status(404).send({
            success: false,
            error: environment.decryptionError,
          });
      }

      let parsed: any;

      try {
        parsed = JSON.parse(query);
      } catch (error) {
        response

          .status(404).send({
            success: false,
            error: environment.decryptionError,
          });
      }

      const userId = tokenVerificationResponse.uid;

      const { id } = parsed;

      const accountEntry = await admin.firestore().collection(`social_accounts`).get();

      // Check if the id exists in some entry
      // If so, check if this userId is the one where it's updated
      // If so, then send success true
      // If no, then send error that this telegram account is linked to some other user
      // If not, check if there is some other id entered in this user entry
      // If so, non-empty entry, send error that your account is linked to some other account
      // If so, empty entry, then update and send success true
      // If not, send success true

      let userEntry = null;

      for (const entry of accountEntry.docs) {
        if (entry.data().telegramId == id) {
          userEntry = entry.id;
        }
      }

      if (userEntry) {

        if (userEntry == userId) {

          response.status(200).send({
            success: true,
          })

        } else {

          response.status(200).send({
            success: false,
            error: "This telegram account is linked to another Jobgrader user"
          })

        }

      } else {

        const userAccount = await admin.firestore().doc(`social_accounts/${userId}`).get();

        if (userAccount.exists) {

          if (userAccount.data().telegramId == "") {
            await admin.firestore().doc(`social_accounts/${userId}`).create({
              discordId: "",
              facebookId: "",
              linkedinId: "",
              telegramId: id,
              twitterId: "",
              instagramId: ""
            });
            response.status(200).send({
              success: true,
              data: {
                discordId: "",
                facebookId: "",
                linkedinId: "",
                telegramId: id,
                twitterId: "",
                instagramId: ""
              }
            })
          } else {
            response.status(200).send({
              success: false,
              error: "Another Telegram account is linked with your user account",
              data: {
                discordId: "",
                facebookId: "",
                linkedinId: "",
                telegramId: id,
                twitterId: "",
                instagramId: ""
              }
            })
          }

        } else {
          await admin.firestore().doc(`social_accounts/${userId}`).create({
            discordId: "",
            facebookId: "",
            linkedinId: "",
            telegramId: id,
            twitterId: "",
            instagramId: ""
          });
          response.status(200).send({
            success: true,
            data: {
              discordId: "",
              facebookId: "",
              linkedinId: "",
              telegramId: id,
              twitterId: "",
              instagramId: ""
            }
          })
        }


      }


    })

  } catch (error) {
    console.error('Error in function:', error);
    response.status(500).json({ error: environment.serverError });
  }

});

exports.walletconnectregistertopic = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  try {

    await authenticate(request, response, async () => {

      const tokenVerificationResponse = await verifyToken(request);

      if (!tokenVerificationResponse) {
        response
          .status(401).send({
            success: false,
            error: environment.tokenError,
          });
      }

      const body: string = request.body as string;

      let cleaned: string;

      try {
        cleaned = decodeURIComponent(body);
      } catch (error) {
        cleaned = body;
      }

      let query;

      try {
        query = await decryptBody(cleaned);
      } catch (error) {
        response
          .status(404).send({
            success: false,
            error: environment.decryptionError,
          });
      }

      let parsed: any;

      try {
        parsed = JSON.parse(query);
      } catch (error) {
        response

          .status(404).send({
            success: false,
            error: environment.decryptionError,
          });
      }

      const { topic, token } = parsed;

      console.log({ topic, token });

      if (token) {
        await admin.firestore().collection(`walletconnect`).add({
          topic,
          token
        });
      }

      response.status(200).send({
        success: true
      })

    });


  } catch (error) {
    console.error('Error in function:', error);
    response.status(500).json({ error: environment.serverError });
  }

});

exports.walletconnectderegistertopic = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  try {

    await authenticate(request, response, async () => {

      const tokenVerificationResponse = await verifyToken(request);

      if (!tokenVerificationResponse) {
        response
          .status(401).send({
            success: false,
            error: environment.tokenError,
          });
      }

      const body: string = request.body as string;

      let cleaned: string;

      try {
        cleaned = decodeURIComponent(body);
      } catch (error) {
        cleaned = body;
      }

      let query;

      try {
        query = await decryptBody(cleaned);
      } catch (error) {
        response
          .status(404).send({
            success: false,
            error: environment.decryptionError,
          });
      }

      let parsed: any;

      try {
        parsed = JSON.parse(query);
      } catch (error) {
        response

          .status(404).send({
            success: false,
            error: environment.decryptionError,
          });
      }

      const { topic } = parsed;

      console.log({ topic });

      const collection = await admin.firestore().collection(`walletconnect`).get();

      let id = null;

      for (const entry of collection.docs) {
        if (entry.data().topic == topic) {
          id = entry.id;
        }
      }

      if (id) {
        await admin.firestore().doc(`walletconnect/${id}`).delete();
      }

      response.status(200).send({
        success: true
      })

    });


  } catch (error) {
    console.error('Error in function:', error);
    response.status(500).json({ error: environment.serverError });
  }

});

exports.walletconnectpush = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  try {

    interface PayloadBody {
      id: string;
      message: string;
      payload: {
        blob: string;
        flags: number;
        topic: string;
      },
      tag: number;
      topic: string;
    }

    interface DecodedBody {
      body: string;
      icon: string;
      title: string;
      url: string;
    }

    // console.log(request.params);
    const payloadBody: PayloadBody = request.body;
    console.log(payloadBody);
    // console.log(request.headers);
    // console.log(request.query);
    // AGN6ICaB3Xwib6B8MLItHNyUAm3Stipemi+0pM/D1Ey81Qa/jHRLE1GHCtNs5hNY/cEx8UJxfFDHq6XPpiJDjfkMWtDtspkimS77hh5VEzXGbORfIC1mTEjjdUMYTPe/eWK+adQYaQX/q4bRyhWJn/NYBjsdk4HjLER4g79H97/OVIPE8TXzkR042cD2gbGpsGtYA3KvHrVuo87A4/Iini9nFHX6BltVCx5HOyJGYWwdtud1+BYEOa+wvHKD9TtLjiGbGmlfVomBtY2BbLQIpnITKPILZstdI3FqXN2HE3sa8n7Bjc5tP7vGiBGbwrwhagAMxCP0ws3BYn8jYyBvurrJgTkCdh1VGHJomwf6YQigdfTzUwPPKtBTc5Ib1Jx9Wpqf/LP7hXhrfQvxrBItZlCIaluArrUgTWTVVUL8HlcDAO4dNG6o9t6tc+8hr52BA/w=

    // const exampleBody = {
    //   id: '898f874b74a27c7545f2cf0d785d0398354ad7dea6629fbe94575d122090c893',
    //   message: 'AGN6ICaB3Xwib6B8MLItHNyUAm3Stipemi+0pM/D1Ey81Qa/jHRLE1GHCtNs5hNY/cEx8UJxfFDHq6XPpiJDjfkMWtDtspkimS77hh5VEzXGbORfIC1mTEjjdUMYTPe/eWK+adQYaQX/q4bRyhWJn/NYBjsdk4HjLER4g79H97/OVIPE8TXzkR042cD2gbGpsGtYA3KvHrVuo87A4/Iini9nFHX6BltVCx5HOyJGYWwdtud1+BYEOa+wvHKD9TtLjiGbGmlfVomBtY2BbLQIpnITKPILZstdI3FqXN2HE3sa8n7Bjc5tP7vGiBGbwrwhagAMxCP0ws3BYn8jYyBvurrJgTkCdh1VGHJomwf6YQigdfTzUwPPKtBTc5Ib1Jx9Wpqf/LP7hXhrfQvxrBItZlCIaluArrUgTWTVVUL8HlcDAO4dNG6o9t6tc+8hr52BA/w=',
    //   payload: {
    //   blob: 'eyJ0aXRsZSI6IlNpZ25hdHVyZSByZXF1aXJlZCIsImJvZHkiOiJZb3UgaGF2ZSBhIG1lc3NhZ2UgdG8gc2lnbiIsImljb24iOm51bGwsInVybCI6bnVsbH0=',
    //   flags: 2,
    //   topic: '481ffdc9c2becb42f72555eb62426a6db16d5e7ecad151583b3bcda7be7fd6e0'
    //   },
    //   tag: 1108,
    //   topic: '481ffdc9c2becb42f72555eb62426a6db16d5e7ecad151583b3bcda7be7fd6e0'
    //   };

    const decodeString = (hashValue: string) => {
      const buffer = Buffer.from(hashValue, 'base64');
      const decodedResponse = buffer.toString('utf-8');
      return decodedResponse;
    }

    try {

      const decoded: DecodedBody = JSON.parse(decodeString(payloadBody.payload.blob));
      const { title, body } = decoded;

      const tokenEntries = await admin.firestore().collection(`walletconnect`).get();

      let token = null;

      for (const tokenEntry of tokenEntries.docs) {
        if (tokenEntry.data().topic == payloadBody.topic) {
          token = tokenEntry.data().token;
        }
      }

      if (token) {
        const message = {
          notification: { title, body },
          token,
        };
        try {
          admin.messaging().send(message);
        } catch (error) {
          console.log(error);
        }
      }
      // {"body":"You have a message to sign", "icon":null, "title":"Signature required", "url":null}

    } catch (e) {

      console.log(e);

    }


    response.status(200).send({
      success: true
    });

  } catch (error) {
    console.error('Error in function:', error);
    response.status(500).json({ error: environment.serverError });
  }

});

exports.discordwebhook = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();
  const discord = (await admin.firestore().doc(`environment/discord`).get()).data();

  // cors(corsOptions)(request, response, async () => {

  try {

    console.log(`url: ${request.url}`);
    console.log(`baseUrl: ${request.baseUrl}`);
    console.log(`originalUrl: ${request.originalUrl}`);

    const grant_type = "authorization_code";
    const redirect_uri = discord.redirectUri;
    const client_id = discord.clientId;
    const client_secret = discord.clientSecret;

    const url = new URL(redirect_uri + request.url);
    const code = url.searchParams.get('code');
    const state = url.searchParams.get('state');

    const tokenUrl = discord.tokenUrl;
    const form = {
      grant_type,
      redirect_uri,
      client_id,
      client_secret,
      code,
    };

    const tokenResponse = await axios.post(tokenUrl, form, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    });

    console.log({ data: tokenResponse.data });

    const token = tokenResponse.data.access_token;
    const refreshToken = tokenResponse.data.refresh_token;
    const scope = tokenResponse.data.scope;
    const expiresIn = tokenResponse.data.expires_in;
    const tokenType = tokenResponse.data.token_type;


    if (!!code && !!state) {
      await admin.firestore().collection(`state_discord`).add({
        code,
        state,
        token,
        refreshToken,
        scope,
        expiresIn,
        tokenType
      });

      response
        .status(200).send({
          success: true
        });
    } else {
      response
        .status(200).send({
          success: false
        });
    }

  } catch (error) {
    console.error('Error in function:', error);
    response.status(500).json({ error: environment.serverError });
  }

  // })


});

exports.obtaindiscordcode = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();
  const discord = (await admin.firestore().doc(`environment/discord`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const body: string = request.body as string;

        let cleaned: string;

        try {
          cleaned = decodeURIComponent(body);
        } catch (error) {
          cleaned = body;
        }

        let query;

        try {
          query = await decryptBody(cleaned);
        } catch (error) {
          response

            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        let parsed: any;

        try {
          parsed = JSON.parse(query);
        } catch (error) {
          response

            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        const { state } = parsed;

        const collection = await admin.firestore().collection(`state_discord`).get();

        let code = null;
        let token = "";
        let id = null;
        let me = null;
        let guilds = null;

        for (const document of collection.docs) {
          if (document.data().state == state) {
            code = document.data().code;
            token = document.data().token;
            id = document.id;

            console.log({ code, token, id })
          }
        }

        if (!!code) {
          const discordApiBaseUrl = discord.discordApiBaseUrl;
          const userInfoUrl = discord.userInfo;
          const getAllGuildsUrl = discord.getAllGuilds;

          const userInfo = await axios.get(discordApiBaseUrl + userInfoUrl, {
            headers: {
              Authorization: `Bearer ${token}`
            }
          })

          me = userInfo.data;

          console.log({ me });

          const getAllGuilds = await axios.get(discordApiBaseUrl + getAllGuildsUrl, {
            headers: {
              Authorization: `Bearer ${token}`
            }
          })

          guilds = getAllGuilds.data;

          console.log({ guilds });

          if (id) {
            await admin.firestore().doc(`state_discord/${id}`).delete();
          }
        }

        response
          .status(200).send({
            success: true,
            code,
            me,
            guilds
          });

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })


});


exports.jobgraderinviteurl = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();
  const discord = (await admin.firestore().doc(`environment/discord`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        response
          .status(200).send({
            success: true,
            inviteUrl: discord.inviteUrl
          });

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })


});

// To be used by the Bot
exports.botfetchsocialmediatasks = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  try {

    const tokenVerificationResponse = await verifyApiKey(request);

    if (!tokenVerificationResponse) {
      response
        .status(401).send({
          success: false,
          error: environment.tokenError,
        });
    }

    const collection = await admin.firestore().collection(`social_media_tasks`).get();

    let data = [];

    for (const document of collection.docs) {
      const { active, earnings, earningsToken, language, shareContent, targetApp, targetChannel, taskCode, taskDescription, inviteUrl } = document.data();

      data.push({
        taskId: document.id,
        active, earnings, earningsToken, language, shareContent, targetApp, targetChannel, taskCode, taskDescription, inviteUrl
      })
    }

    response
      .status(200).send({
        success: true,
        data
      });

  } catch (error) {
    console.error('Error in function:', error);
    response.status(500).json({ error: environment.serverError });
  }

});

// To be used by the Bot
exports.botfetchsocialuserstatus = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  try {

    const tokenVerificationResponse = await verifyApiKey(request);

    if (!tokenVerificationResponse) {
      response
        .status(401).send({
          success: false,
          error: environment.tokenError,
        });
    }

    let userId = null;
    const { discordId, telegramId } = request.body;

    const socialAccounts = await admin.firestore().collection(`social_accounts`).get();

    for (const socialAccount of socialAccounts.docs) {
      if (discordId) {
        if (socialAccount.data().discordId == discordId) {
          userId = socialAccount.id;
        }
      }

      if (telegramId) {
        if (socialAccount.data().telegramId == telegramId) {
          userId = socialAccount.id;
        }
      }
    }

    if (!userId) {
      response
        .status(200).send({
          success: false,
          error: 'Record not found',
        });
    } else {

      const collection = await admin.firestore().collection(`social_user_status`).get();

      let data = [];

      for (const doc of collection.docs) {
        if (doc.data().userId == userId) {
          const { earnings, earningsToken, status, taskId, timestamp, userId } = doc.data();
          data.push(
            { earnings, earningsToken, status, taskId, timestamp, userId }
          );
        }
      }

      response
        .status(200).send({
          success: true,
          data
        })

    }

  } catch (error) {
    console.error('Error in function:', error);
    response.status(500).json({ error: environment.serverError });
  }

});

// To be used by the Bot
exports.botupdatesocialuserstatus = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  try {

    const tokenVerificationResponse = await verifyApiKey(request);

    console.log(tokenVerificationResponse);

    if (!tokenVerificationResponse) {
      response
        .status(401).send({
          success: false,
          error: environment.tokenError,
        });
    }

    console.log(request.body);

    const { discordId, telegramId, taskId, status, timestamp, earnings, earningsToken } = request.body;
    let userId = null;

    const socialAccounts = await admin.firestore().collection(`social_accounts`).get();

    for (const socialAccount of socialAccounts.docs) {
      if (telegramId) {
        if (socialAccount.data().telegramId == telegramId) {
          userId = socialAccount.id;
        }
      }

      if (discordId) {
        if (socialAccount.data().discordId == discordId) {
          userId = socialAccount.id;
        }
      }

    }

    if (!userId) {
      response
        .status(200).send({
          success: false,
          error: 'Record not found',
        });
    } else {
      await admin.firestore().collection(`social_user_status`).add({
        userId,
        taskId,
        status,
        timestamp: convertTimestamp(timestamp),
        earnings,
        earningsToken
      });

      response
        .status(200).send({
          success: true,
        })
    }

  } catch (error) {
    console.error('Error in function:', error);
    response.status(500).json({ error: environment.serverError });
  }


});

// To be used by the bot
exports.botfetchsocialaccounts = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  try {

    const tokenVerificationResponse = await verifyApiKey(request);

    console.log(tokenVerificationResponse);

    if (!tokenVerificationResponse) {
      response
        .status(401).send({
          success: false,
          error: environment.tokenError,
        });
    }

    const { telegramId } = request.body;

    const collection = await admin.firestore().collection(`social_accounts`).get();

    let userId = null;

    for (const doc of collection.docs) {
      if (doc.data().telegramId == telegramId) {
        userId = doc.id;
      }
    }

    if (userId) {
      response.status(200).send({
        success: true
      });
    } else {
      response.status(200).send({
        success: false
      });
    }


  } catch (error) {
    console.error('Error in function:', error);
    response.status(500).json({ error: environment.serverError });
  }

});


exports.checkjobsolution = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const body: string = request.body as string;

        let cleaned: string;

        try {
          cleaned = decodeURIComponent(body);
        } catch (error) {
          cleaned = body;
        }

        let query;

        try {
          query = await decryptBody(cleaned);
        } catch (error) {
          response

            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        let parsed: any;

        try {
          parsed = JSON.parse(query);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        const { escrowAddress, solution, address, deviceId, epn, certificates } = parsed;

        const solutions = (await admin.firestore().doc(`job_solutions/quiz`).get()).data().solutions;

        const nftId = certificates[0] ?? null;

        const checkAvailability = !!certificates ? solutions.find(s => s.nftId.toLowerCase() == nftId.toLowerCase()) : solutions.find(s => s.escrowAddress.toLowerCase() == escrowAddress.toLowerCase());

        if (!checkAvailability) {
          response
            .status(200).send({
              success: false,
            });
        } else {

          const userId = tokenVerificationResponse.uid;

          const basicDataUrl = `${process.env.MW_API_URL}/user/basicdata/getbyuserid/${userId}`;
          const trustUrl = `${process.env.MW_API_URL}/api/v1/user/${userId}/trust`;

          try {
            const blueToken = await generateBlueTokens();
            const basicDataResponse = await axios.get(basicDataUrl, {
              headers: {
                "x-api-key": blueToken.encrypted,
                Authorization: request.headers.authorization
              }
            });
            const trustResponse = await axios.get(trustUrl, {
              headers: {
                "x-api-key": blueToken.encrypted,
                Authorization: request.headers.authorization,
                "Content-Type": "application/json"
              }
            });

            const basicData = JSON.parse(symmetricDecrypt(basicDataResponse.data, blueToken.key));
            const trust = JSON.parse(symmetricDecrypt(trustResponse.data, blueToken.key));

            const user = Object.assign({}, basicData, mapTrustData(trust));
            console.log({ user });

            const trusted = (user.firstnameStatus == 1 && user.lastnameStatus == 1);
            const solutionsCorrect = checkSolutionEquality(checkAvailability.solution, solution, checkAvailability.minimumCorrectAnswersRequired);

            if (trusted && solutionsCorrect) {
              try {
                mintsoulbound(solutions, address, userId, nftId, user, checkAvailability, deviceId, epn);
              } catch (error) {
                console.log(error);
              }
            }

            response
              .status(200).send({
                success: true,
                trusted,
                solutionsCorrect
              });

          } catch (e) {
            response
              .status(200).send({
                success: false,
              });
          }
        }
      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })

});

const mintsoulbound = (
  solutions: any, address: string,
  userId: string, nftId: string,
  user: any,
  checkAvailability: any,
  deviceId: string,
  epn?: boolean) => {

  const email = user.email;

  mintAssessmentNft(address, userId, email, epn, nftId).then(status => {
    if (status) {
      try {
        solutions.find(s => s.nftId.toLowerCase() == nftId.toLowerCase()).mintedAddresses.push(address)
        admin.firestore().doc(`job_solutions/quiz`).update({ solutions });
      } catch (error) {
        console.log(error);
      }
      const imagePath = 'assets/images/Certificate_Jobgrader.png';
      const name = `${!!user ? user.firstname : ''} ${!!user ? user.lastname : ''}`.trim();
      const title = checkAvailability.certificateTitle;
      const nameFontPath = 'assets/fonts/4oradnfaSpXNPF6OPQLF8Zjj.ttf.fnt';
      const titleFontPath = 'assets/fonts/0xjsmzsGyWg257QRSYAQbDQK.ttf.fnt';

      const firstname = user.firstname.toLowerCase();
      const modifiedFirstname = firstname.at(0).toUpperCase() + firstname.slice(1);
      addTextToImage({ imagePath, name, title, nameFontPath, titleFontPath }, email, modifiedFirstname);
      sendPushNotification("Your Soulbound NFT!", `Congratulations! Your soulbound NFT has been minted, and you receive the ${title} certificate!`, userId, deviceId);

    }
  })

};

exports.storeassessmentvcs = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const body: string = request.body as string;

        let cleaned: string;

        try {
          cleaned = decodeURIComponent(body);
        } catch (error) {
          cleaned = body;
        }

        let query;

        try {
          query = await decryptBody(cleaned);
        } catch (error) {
          response

            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        let parsed: any;

        try {
          parsed = JSON.parse(query);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        const userId = tokenVerificationResponse.uid;
        const { vcId } = parsed;

        const document = await admin.firestore().doc(`assessment_vcs/${userId}`).get();

        if (document.exists) {

          let vcIds = document.data().vcIds;

          const check = vcIds.includes(vcId);

          if (!check) {
            vcIds.push(vcId);
            await admin.firestore().doc(`assessment_vcs/${userId}`).update({
              vcIds
            });
          }
        } else {
          await admin.firestore().doc(`assessment_vcs/${userId}`).create({
            vcIds: [vcId]
          });
        }

        response
          .status(200).send({
            success: true,
          });

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })

});

exports.obtainassessmentvcs = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const userId = tokenVerificationResponse.uid;

        const document = await admin.firestore().doc(`assessment_vcs/${userId}`).get();

        let vcIds = [];

        if (document.exists) {
          vcIds = document.data().vcIds;
        }

        response
          .status(200).send({
            success: true,
            vcIds
          });

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })

});

exports.fetchassessmentcontract = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const epn = !!request.query.epn ? (request.query.epn == "true") : false;

        const document = (await admin.firestore().doc(`environment/assessment`).get()).data();
        const solutions = (await admin.firestore().doc(`job_solutions/quiz`).get()).data().solutions;

        let jobNft = [];

        for (const solution of solutions) {
          const { nftId } = solution;
          jobNft.push({ nftId });
        }

        // Comment the following out when the demonstration is finished:
        jobNft.push({ nftId: "f6ccfca8-56fc-4675-a9ba-37e0383b07b2" });

        response
          .status(200).send({
            success: true,
            contract: !epn ? document.contract : "0x35003C3827cB7ac45ca7d61584b153b6C8ee0DfA",
            jobNft
          });


      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })

});

// exports.testmail = functions.https.onRequest(opts, async (request, response) => {
//   const imagePath = 'assets/images/Certificate_Jobgrader.png';
//   const name = "KALINGAPUTRA MAHAMEGHAVAHANA";
//   const title = "Kalingaputra Mahameghavahana";
//   const nameFontPath = 'assets/fonts/4oradnfaSpXNPF6OPQLF8Zjj.ttf.fnt';
//   const titleFontPath = 'assets/fonts/0xjsmzsGyWg257QRSYAQbDQK.ttf.fnt';       

//   addTextToImage({ imagePath, name, title, nameFontPath, titleFontPath}, "ansik@blockchain-helix.com", "Ansik");
//   response.status(200).send({ success: true });
// })

exports.fetchepnnfts = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const body: string = request.body as string;

        let cleaned: string;

        try {
          cleaned = decodeURIComponent(body);
        } catch (error) {
          cleaned = body;
        }

        let query;

        try {
          query = await decryptBody(cleaned);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        let parsed: any;

        try {
          parsed = JSON.parse(query);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        const { address } = parsed;

        let nfts: Array<any> = [];

        try {
          nfts = await fetchEpnNfts(address);
        } catch (error) {
          console.log(error);
        }

        response
          .status(200).send({
            success: true,
            nfts
          });

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });
    }

  })

});

exports.thxcusd = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {


    try {
      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const params = { pair: "THXC/USD" }
        const config = {
          method: 'get',
          url: 'https://exchange-api.lcx.com/api/ticker',
          headers: {
            'Content-Type': 'application/json',
          },
          params
        }

        try {

          const r = await axios(config);
          const { bestAsk, bestBid } = r.data.data;

          response.status(200).send({
            success: true,
            data: { bestAsk, bestBid }
          });


        } catch (error) {
          response.status(200).send({
            success: true,
            data: { bestAsk: 0.05, bestBid: 0.05 }
          });
        }

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });

    }

  })

});

// exports.csvparser = functions.https.onRequest(opts, async (request, response) => {

//   const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

//   cors(corsOptions)(request, response, async () => {

//     try {

//         const tableContent = (await admin.firestore().doc(`waitlist/deu`).get()).data().emails;

//         let data = [
//           
//         ];

//         data.concat(tableContent);

//         await admin.firestore().doc(`waitlist/deu`).update({
//           emails: data
//         })


//         response.status(200).send({
//           success: true,
//           tableContent
//         });

//     } catch (error) {
//       console.error('Error in function:', error);
//       response.status(500).json({ error: environment.serverError }); 

//     }

//   })

// });

// exports.testmailer = functions.https.onRequest(opts, async (request, response) => {

//   const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

//   cors(corsOptions)(request, response, async () => {

//     try {

//       await sendEmailToReferred("ansik+mailtester@blockchain-helix.com", "Ansik")
//       await sendEmailToReferred("ansikmahapatra@outlook.com", "Ansik")

//       response.status(200).send({
//         success: true,
//       });

//     } catch (error) {
//       console.error('Error in function:', error);
//       response.status(500).json({ error: environment.serverError }); 

//     }

//   })

// });

exports.mailchimpwebhook = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();


  try {
    console.log(request.params);
    const listId = request.params[0];
    console.log(listId);

    if (!!request.body.data) {
      if (!!request.body.data.email) {
        const email = request.body.data.email;
        const type = request.body.type;

        // targetApp: mailchimp
        // targetChannel: listId
        // taskCode: subscribe

        // Add push notification

        const collection = await admin.firestore().collection(`social_media_tasks`).get();

        let earnings = null;
        let earningsToken = null;
        let taskId = null;
        let userId = null;

        for (const document of collection.docs) {
          const entry = document.data();
          if (entry.taskCode == "subscribe" && entry.targetChannel == listId && entry.targetApp == "mailchimp") {
            taskId = document.id;
            earnings = entry.earnings;
            earningsToken = entry.earningsToken;
          }
        }

        if (taskId) {
          const userCollection = await admin.firestore().collection(`users`).get();
          for (const users of userCollection.docs) {
            if (users.data().email == email && type == "subscribe") {
              userId = users.id;

              // Check if the user has already subscribed
              const checkCollection = await admin.firestore().collection(`social_user_status`).get();

              let checker = null;

              for (const checkDocs of checkCollection.docs) {
                if (checkDocs.data().taskId == taskId && checkDocs.data().userId == userId) {
                  checker = checkDocs.id;
                }
              }

              if (!checker) {

                // Add earnings to the social_user_status table
                await admin.firestore().collection(`social_user_status`).add({
                  earnings,
                  earningsToken,
                  status: 2,
                  taskId,
                  timestamp: convertTimestamp(+new Date()),
                  userId
                });

                sendPushNotification("Newsletter Subscription", "Congratulations! You have successfully subscribed to the newsletter", userId);

              }
            }
          }
        }

        response.status(200).send({
          success: true,
        });
      } else {
        response.status(200).send({
          success: false,
        });
      }
    } else {
      response.status(200).send({
        success: false,
      });
    }

  } catch (error) {
    console.error('Error in function:', error);
    response.status(500).json({ error: environment.serverError });

  }

});

exports.subscribemailchimp = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const userId = tokenVerificationResponse.uid;

        const body: string = request.body as string;

        let cleaned: string;

        try {
          cleaned = decodeURIComponent(body);
        } catch (error) {
          cleaned = body;
        }

        let query;

        try {
          query = await decryptBody(cleaned);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        let parsed: any;

        try {
          parsed = JSON.parse(query);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        const { taskId } = parsed;
        const user = (await admin.firestore().doc(`users/${userId}`).get()).data();
        const task = (await admin.firestore().doc(`social_media_tasks/${taskId}`).get()).data();

        const listId = task.targetChannel;
        const { email } = user;

        // Add the code to check prior-subscription

        const API_SERVER = "us13"; //us21

        const API_KEY = environment.mailchimpApiKey;
        const url = `https://${API_SERVER}.api.mailchimp.com/3.0/lists/${listId}/members`;

        const data = {
          email_address: email,
          status: 'subscribed'
        };

        console.log({ url, email, API_KEY, data });

        try {
          const re = await axios.post(url, data, {
            headers: {
              'Authorization': `apikey ${API_KEY}`,
              'Content-Type': 'application/json'
            }
          });

          console.log(re.data);

          // Add the code to update the task

          await admin.firestore().collection(`social_user_status`).add({
            earnings: task.earnings,
            earningsToken: task.earningsToken,
            status: 2,
            taskId,
            timestamp: +new Date(),
            userId
          })

          response.status(200).send({
            success: true,
          });
        } catch (error) {
          console.log(error);

          response.status(200).send({
            success: false,
          });
        }

      })

    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });

    }

  })

});

exports.checknftmintingstatus = functions.https.onRequest(opts, async (request, response) => {

  const environment = (await admin.firestore().doc(`environment/prod`).get()).data();

  cors(corsOptions)(request, response, async () => {

    try {

      await authenticate(request, response, async () => {

        const tokenVerificationResponse = await verifyToken(request);

        if (!tokenVerificationResponse) {
          response
            .status(401).send({
              success: false,
              error: environment.tokenError,
            });
        }

        const body: string = request.body as string;

        console.log(body);

        let cleaned: string;

        try {
          cleaned = decodeURIComponent(body);
        } catch (error) {
          cleaned = body;
        }

        console.log(cleaned);

        let query;

        try {
          query = await decryptBody(cleaned);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        let parsed: any;

        console.log(query);

        try {
          parsed = JSON.parse(query);
        } catch (error) {
          response
            .status(404).send({
              success: false,
              error: environment.decryptionError,
            });
        }

        const userId = tokenVerificationResponse.uid;

        const { address } = parsed;

        const solutions = (await admin.firestore().doc(`job_solutions/quiz`).get()).data().solutions;
        const mintingLogs = (await admin.firestore().doc(`minting_logs/${userId}`).get());

        let results = {};
        let status = "NOT_STARTED";

        for (const solution of solutions) {

          if (mintingLogs.exists) {
            if (mintingLogs.data().mints.length > 0) {
              const item = mintingLogs.data().mints.find(m => m.nftId == solution.nftId);
              if (item) {
                if (item.status == "Error in Minting") {
                  status = "MINTING_ERROR";
                } else if (item.status == "Minting Started") {
                  status = "MINTING_PROGRESS";
                }
              }
            }
          }

          if (Array.from(solution.mintedAddresses, (k: string) => k.toLowerCase()).includes(address.toLowerCase())) {
            status = "MINTING_DONE";
          }
          results = Object.assign({ [solution.nftId]: status })
        }

        response.status(200).send({
          success: true,
          results
        })

      })
    } catch (error) {
      console.error('Error in function:', error);
      response.status(500).json({ error: environment.serverError });

    }

  })

});
